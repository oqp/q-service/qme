import axios from 'axios'
import ENV from './env'

const createApiInstance = headers => axios.create({
  baseURL: ENV.API_URL,
  headers,
})

const handleResponse = (response) => {
  if (response.data) {
    return {
      status: response.status,
      data: response.data,
      headers: response.headers,
    }
  }
  return { status: response.status, headers: response.headers }
}

const catchError = (e) => {
  if (e.response) {
    return {
      status: e.response.status,
      data: e.response.data,
      headers: e.response.headers,
    }
  } if (e.request) {
    throw new Error(e.request)
  } else {
    throw new Error(e.message)
  }
}

export default {
  get: (path, headers = {}) => (
    createApiInstance(headers)
      .get(path)
      .then(handleResponse)
      .catch(catchError)
  ),
  post: (path, body = {}, headers = {}) => (
    createApiInstance()
      .request({
        url: path,
        method: 'POST',
        headers,
        data: body,
      })
      .then(handleResponse)
      .catch(catchError)
  ),
  put: (path, body = {}, headers = {}) => (
    createApiInstance()
      .request({
        url: path,
        method: 'PUT',
        headers,
        data: body,
      })
      .then(handleResponse)
      .catch(catchError)
  ),
  delete: (path, body = {}) => (
    createApiInstance()
      .request({
        url: path,
        method: 'DELETE',
        data: body,
      })
      .then(handleResponse)
      .catch(catchError)
  ),
  patch: (path, body = {}) => (
    createApiInstance()
      .request({
        url: path,
        method: 'PATCH',
        data: body,
      })
      .then(handleResponse)
      .catch(catchError)
  ),
}
