import moment from 'moment'

const emailRegEx = /^[a-zA-Z0-9ก-๛.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9ก-๛](?:[a-zA-Z0-9ก-๛-]{0,61}[a-zA-Z0-9ก-๛])?(?:\.[a-zA-Z0-9ก-๛](?:[a-zA-Z0-9ก-๛-]{0,61}[a-zA-Z0-9ก-๛])?)*$/
const isNumberRegEx = /^\d*$/
const isValidURLRegEx = /^(https?):\/\/[^\s$.?#].[^\s]*$/
const isValidSubdomain = /^[a-zA-Z0-9-]+$/
const isSubdomainContainReservedWord = /^www(\d*)$/
const isValidPhoneNumber = /\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/
const isValidDomainName = /^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$/

export default class FormValidation {
  static loginForm(values) {
    const errors = {}
    if (!values.email) {
      errors.email = 'Please enter your email'
    } else if (!emailRegEx.test(values.email)) {
      errors.email = 'Please enter e-mail in format: yourname@example.com'
    }
    if (!values.password) {
      errors.password = 'Please enter your password'
    }
    return errors
  }

  static signupForm(values) {
    const errors = {}
    if (!values.firstname) {
      errors.firstname = 'Please enter first name'
    }
    if (!values.lastname) {
      errors.lastname = 'Please enter last name'
    }
    if (!values.telno) {
      errors.telno = 'Please enter phone number'
    } else if (!isValidPhoneNumber.test(values.telno)) {
      errors.telno = 'Invalid phone number format'
    }
    if (!values.email) {
      errors.email = 'Please enter e-mail'
    } else if (!emailRegEx.test(values.email)) {
      errors.email = 'Please enter e-mail in format: yourname@example.com'
    }
    if (!values.acceptTerm) {
      errors.acceptTerm = 'Please accept our Agreement'
    }
    if (!values.password) {
      errors.password = 'This field is required'
    } else if (values.password.length < 8) {
      errors.password = 'Password must be at least 8 characters'
    }
    if (values.password && !values.confirmPassword) {
      errors.confirmPassword = 'The specified password do not match'
    }
    if (values.password && values.confirmPassword && (values.password !== values.confirmPassword)) {
      errors.confirmPassword = 'Password and confirm password isn\'t match'
    }
    return errors
  }

  static createWaitingRoomForm(values) {
    const errors = {}
    if (!values.name) {
      errors.name = 'This field is required'
    } else if (!values.name.trim()) {
      errors.name = 'Must not contain only whitespace(s)'
    }
    if (!values.website) {
      errors.website = 'This field is required'
    } else if (!isValidDomainName.test(values.website)) {
      errors.website = 'Please enter a valid domain name'
    }
    return errors
  }

  static eventForm(values) {
    const errors = {}
    if (!values.name) {
      errors.name = 'This field is required'
    } else if (!values.name.trim()) {
      errors.name = 'Must not contain only whitespace(s)'
    }
    if (!values.maxOutflowAmount) {
      errors.maxOutflowAmount = 'Please specify'
    } else if (!isNumberRegEx.test(values.maxOutflowAmount)) {
      errors.maxOutflowAmount = 'Must be positive integer'
    }
    if (!values.redirectURL) {
      errors.redirectURL = 'Please specify'
    } else if (!isValidURLRegEx.test(values.redirectURL)) {
      errors.redirectURL = 'URL should start with \'http://\' or \'https://\''
    }
    if (!values.sessionTime) {
      errors.sessionTime = 'Please specify'
    } else if (!isNumberRegEx.test(values.sessionTime)) {
      errors.sessionTime = 'Must be positive integer'
    }
    if (!values.openTime) {
      errors.openTime = 'Please specify'
    }
    if (!values.closeTime) {
      errors.closeTime = 'Please specify'
    }
    if ((values.openTime && !values.closeTime) || (values.closeTime && !values.openTime)) {
      errors.closeTime = 'Please specify both Close time and Open Time'
      errors.openTime = 'Please specify both Close time and Open Time'
    }
    if (values.openTime && values.closeTime) {
      if (!moment(values.openTime).isSameOrBefore(moment(values.closeTime))) {
        errors.closeTime = 'Close time must be after Open time'
      }
    }
    if (!values.subdomain) {
      errors.subdomain = 'Please enter subdomain'
    } else if (isSubdomainContainReservedWord.test(values.subdomain)) {
      errors.subdomain = `${values.subdomain} is reserved. Please use other subdomain name`
    } else if (!isValidSubdomain.test(values.subdomain)) {
      errors.subdomain = 'Invalid subdomain format. Only \'a-z\', \'A-Z\', \'0-9\' and \'-\' allowed'
    }
    if (!values.arrivalTime) {
      errors.arrivalTime = 'Please specify'
    } else if (!isNumberRegEx.test(values.arrivalTime)) {
      errors.arrivalTime = 'Must be integer'
    }
    return errors
  }
}
