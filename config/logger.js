/* eslint-disable no-console */
export default {
  error: (err) => {
    console.error(err)
  },
  info: (data) => {
    console.info(data)
  },
  debug: (data) => {
    console.debug(data)
  },
}
