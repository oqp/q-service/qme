import { FORM_ERROR } from 'final-form'
import cookie from 'js-cookie'
import moment from 'moment'
import axios from '../config/axios'

import logger from '../config/logger'

const sessionKey = 'qme_session'
const signupWithEmail = async ({
  firstname, lastname, email, telno, password, confirmPassword, acceptTerm,
}) => {
  const requestBody = {
    firstname, lastname, email, telno, password, confirmPassword, acceptTerm,
  }
  try {
    const response = await axios.post('/user/register/email', requestBody)
    if (response.status !== 200) {
      if (response.data.code === 4003) {
        return { email: 'This email is already taken' }
      }
      if (response.data.code === 4004) {
        return { confirmPassword: 'Password mismatch' }
      }
      if (response.data.code === 4005) {
        return { acceptTerm: 'Please accept our agreement' }
      }
      return { [FORM_ERROR]: response.data.message }
    }
    logger.debug(response.data)
    window.location = '/login'
    return null
  } catch (err) {
    logger.error(err)
  }
  return { [FORM_ERROR]: 'Opps! Something went wrong.' }
}

const loginWithEmail = async ({ email, password, remember }) => {
  const requestBody = { email, password }
  try {
    const response = await axios.post('/user/login/email', requestBody)
    if (response.status === 401) {
      return { password: 'E-mail or password you entered is incorrect' }
    }
    if (response.status !== 200) {
      return { [FORM_ERROR]: response.data.message }
    }
    const { accessToken: token } = response.data.data
    cookie.set(sessionKey, token, { expires: remember ? 7 : undefined, path: '/' })
    if (remember) {
      cookie.set('remember', 'yes', { expires: remember ? 7 : undefined, path: '/' })
    }
    window.location = '/panel/waiting-room'
    return null
  } catch (err) {
    logger.error(err)
  }
  return { [FORM_ERROR]: 'Opps! Something went wrong.' }
}

const signout = () => {
  cookie.remove(sessionKey)
  cookie.remove('remember')
  window.location = '/login'
}

const extendSession = async () => {
  const accessToken = cookie.get(sessionKey)
  const response = await axios.get('/user/refreshToken', { Authorization: `Bearer ${accessToken}` })
  if (response.status !== 200) {
    logger.info('session expired')
    return false
  }
  const newToken = response.data.data.accessToken
  cookie.remove(sessionKey, { path: '/' })
  const isRemember = cookie.get('remember')
  let cookieExpireDate
  if (isRemember) {
    cookieExpireDate = moment().add(7, 'd').toDate()
  }
  cookie.set(sessionKey, newToken, { path: '/', expires: cookieExpireDate })
  return true
}

export default {
  signupWithEmail,
  loginWithEmail,
  signout,
  extendSession,
}
