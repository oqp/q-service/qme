import cookie from 'js-cookie'
import { FORM_ERROR } from 'final-form'

import axios from '../config/axios'
import logger from '../config/logger'

const getEventList = async (token, waitingRoomId) => {
  if (!token) {
    token = cookie.get('qme_session')
  }
  try {
    const response = await axios.get(`/waitingRoom/${waitingRoomId}/event`, {
      authorization: `Bearer ${token}`,
    })
    if (response.status !== 200) {
      logger.error(response)
      return null
    }
    logger.info(response)
    return response.data.data
  } catch (err) {
    logger.error(err)
  }
  return null
}

const createEvent = async (name, openTime, closeTime, maxOutflowAmount, redirectURL, sessionTime, arrivalTime, isActive, subdomain, waitingRoomId) => {
  arrivalTime *= 60
  sessionTime *= 60
  const requestBody = {
    name, openTime, closeTime, maxOutflowAmount, redirectURL, sessionTime, arrivalTime, isActive, subdomain,
  }
  try {
    const token = cookie.get('qme_session')
    const response = await axios.post(`/waitingRoom/${waitingRoomId}/event`, requestBody, {
      authorization: `Bearer ${token}`,
    })
    if (response.status !== 201) {
      if (response.data.code === 4020) {
        return { subdomain: 'This subdomain is already taken' }
      }
      if (response.data.code === 4021) {
        return { subdomain: 'Invalid subdomain format. Only \'a-z\', \'A-Z\', \'0-9\' and \'-\' allowed' }
      }
      return { [FORM_ERROR]: response.data.message }
    }
    logger.info(response)
    const event = response.data.data
    window.location = `/panel/waiting-room/${waitingRoomId}/event/${event.id}/dashboard`
    return null
  } catch (err) {
    logger.error(err)
  }
  return { [FORM_ERROR]: 'Opps! Something went wrong.' }
}

const editEvent = async (id, name, openTime, closeTime, maxOutflowAmount, redirectURL, sessionTime, arrivalTime, isActive, subdomain, waitingRoomId) => {
  arrivalTime *= 60
  sessionTime *= 60
  const requestBody = {
    name, openTime, closeTime, maxOutflowAmount, redirectURL, sessionTime, arrivalTime, isActive, subdomain,
  }
  try {
    const token = cookie.get('qme_session')
    const response = await axios.put(`/waitingRoom/${waitingRoomId}/event/${id}`, requestBody, {
      authorization: `Bearer ${token}`,
    })
    if (response.status !== 200) {
      if (response.data.code === 4020) {
        return { subdomain: 'This subdomain is already taken' }
      }
      if (response.data.code === 4021) {
        return { subdomain: 'Invalid subdomain format. Only \'a-z\', \'A-Z\', \'0-9\' and \'-\' allowed' }
      }
      return { [FORM_ERROR]: response.data.message }
    }
    logger.info(response)
    window.location = `/panel/waiting-room/${waitingRoomId}`
    return null
  } catch (err) {
    logger.error(err)
  }
  return { [FORM_ERROR]: 'Opps! Something went wrong.' }
}

const getEvent = async (eventId, waitingRoomId, token) => {
  if (!token) {
    token = cookie.get('qme_session')
  }
  try {
    const response = await axios.get(`/waitingRoom/${waitingRoomId}/event/${eventId}`, {
      authorization: `Bearer ${token}`,
    })
    if (response.status !== 200) {
      logger.error(response)
      return null
    }
    logger.info(response)
    return response.data.data
  } catch (err) {
    logger.error(err)
  }
  return null
}

const generateToken = async (eventId, waitingRoomId) => {
  const generateError = { [FORM_ERROR]: 'Cannot generate token. Please try again later' }
  const token = cookie.get('qme_session')
  try {
    const response = await axios.post(`/waitingRoom/${waitingRoomId}/event/${eventId}/integrateToken`, {}, {
      authorization: `Bearer ${token}`,
    })
    if (response.status !== 201) {
      logger.error(response)
      return generateError
    }
    logger.info(response)
    return response.data.data.integrateToken
  } catch (err) {
    logger.error(err)
  }
  return generateError
}

const setActiveStatus = async (eventId, waitingRoomId, isActive) => {
  const token = cookie.get('qme_session')
  try {
    const response = await axios.post(`/waitingRoom/${waitingRoomId}/event/${eventId}/isActive`, {
      isActive,
    }, {
      authorization: `Bearer ${token}`,
    })
    if (response.status !== 200) {
      logger.error(response)
      return false
    }
    return response.data
  } catch (err) {
    logger.error(err)
  }
  return false
}

const getMetric = async (eventId, waitingRoomId) => {
  const token = cookie.get('qme_session')
  try {
    const response = await axios.get(`/waitingRoom/${waitingRoomId}/event/${eventId}/metrics`, {
      authorization: `Bearer ${token}`,
    })
    if (response.status === 200) {
      return response.data.data
    }
  } catch (err) {
    logger.error(err)
  }
  return null
}

export default {
  getEventList,
  createEvent,
  editEvent,
  getEvent,
  generateToken,
  setActiveStatus,
  getMetric,
}
