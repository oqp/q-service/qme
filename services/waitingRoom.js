import cookie from 'js-cookie'
import { FORM_ERROR } from 'final-form'

import axios from '../config/axios'
import logger from '../config/logger'

const getWaitingRoomList = async (token) => {
  if (!token) {
    token = cookie.get('qme_session')
  }
  try {
    const response = await axios.get('/waitingRoom', {
      authorization: `Bearer ${token}`,
    })
    if (response.status !== 200) {
      return null
    }
    logger.debug(response)
    return response.data.data
  } catch (err) {
    logger.error(err)
  }
  return null
}

const createWaitingRoom = async (name, description, website) => {
  const requestBody = {
    name, description, website,
  }
  try {
    const token = cookie.get('qme_session')
    const response = await axios.post('/waitingRoom', requestBody, {
      authorization: `Bearer ${token}`,
    })
    if (response.status !== 201) {
      return { [FORM_ERROR]: response.data.message }
    }
    logger.info(response)
    const waitingRoom = response.data.data
    window.location = `/panel/waiting-room/${waitingRoom.id}`
    return null
  } catch (err) {
    logger.error(err)
  }
  return { [FORM_ERROR]: 'Opps! Something went wrong.' }
}

const getWaitingRoom = async (waitingRoomId) => {
  const token = cookie.get('qme_session')
  try {
    logger.info(`getting waiting room id${waitingRoomId}`)
    const response = await axios.get(`/waitingRoom/${waitingRoomId}`, {
      authorization: `Bearer ${token}`,
    })
    if (response.status === 200) {
      return response.data.data
    }
  } catch (err) {
    logger.error(err)
  }
  return null
}

const editWaitingRoom = async (id, name, description, website) => {
  const requestBody = {
    name, description, website,
  }
  try {
    const token = cookie.get('qme_session')
    const response = await axios.put(`/waitingRoom/${id}`, requestBody, {
      authorization: `Bearer ${token}`,
    })
    if (response.status !== 200) {
      return { [FORM_ERROR]: response.data.message }
    }
    logger.info(response)
    window.location = '/panel/waiting-room'
    return null
  } catch (err) {
    logger.error(err)
  }
  return { [FORM_ERROR]: 'Opps! Something went wrong.' }
}

export default {
  getWaitingRoomList,
  getWaitingRoom,
  createWaitingRoom,
  editWaitingRoom,
}
