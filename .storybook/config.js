import { configure, addParameters, addDecorator } from '@storybook/react'
import { create } from '@storybook/theming'
import { withA11y } from '@storybook/addon-a11y'
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport'
import { withThemesProvider } from 'storybook-addon-styled-component-theme'

import defaultTheme from '../theme/default'

import '../static/css/bootstrap.min.css'

addDecorator(withA11y)

addParameters({
  options: {
    theme: create({
      base: 'light',
      brandTitle: 'Q-Me Components Library',
    })
  }
})

const newViewports = {
  laptop: {
    name: 'Laptop',
    styles: {
      width: '1440px',
      height: '805px',
    }
  },
  desktop: {
    name: 'Desktop',
    styles: {
      width: '1920px',
      height: '1000px',
    }
  }
}

addParameters({
  viewport: {
    viewports: {
      ...INITIAL_VIEWPORTS,
      ...newViewports,
    },
  },
})

const themes = [defaultTheme, {name: 'Test', primaryTextColor: 'red'}]

addDecorator(withThemesProvider(themes))

const loadStories = () => {
  const req = require.context('../stories', true, /\.stories\.js$/);
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);