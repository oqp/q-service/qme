require('dotenv').config({ debug: process.env.DEBUG })

const withCSS = require('@zeit/next-css')

const nextConfig = {
  serverRuntimeConfig: {
    // SECRET: 'thisissecret',
  },
  publicRuntimeConfig: {
    API_URL: process.env.API_URL,
    QCLERK_HOSTNAME: process.env.QCLERK_HOSTNAME,
  },
  webpack(config) {
    config.node = { fs: 'empty' }
    return config
  },
}

module.exports = withCSS(nextConfig)
