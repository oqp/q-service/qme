import styled from 'styled-components'

export const MainButton = styled.button`
  background-color: #FFF;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
  border: 1px solid ${props => props.theme.colors.yellow.default};
  width: ${props => (props.block ? '100%' : 'auto')};
  padding: ${props => (props.block ? 'initial' : '0 42px')};
  height: 30px;
  color: ${props => props.theme.colors.yellow.default};
  text-align: center;
  font-size: 12px;
  transition: all .1s linear;
  &:hover {
    background-color: ${props => (props.theme.colors.yellow.default)};
    color: #FFF;
    border-color: ${props => (props.theme.colors.yellow.default)};
  }
  &:focus {
    background-color: ${props => (props.theme.colors.yellow.focus)};
    color: #FFF;
    border-color: ${props => (props.theme.colors.yellow.focus)};
  }
  &:disabled {
    background-color: #FFF;
    color: ${props => (props.theme.colors.yellow.disabled)};
    border-color: ${props => (props.theme.colors.yellow.disabled)};
    cursor: not-allowed;
  }
  .input-group-append & {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    height: auto;

    &:disabled {
      box-shadow: initial;
    }
  }
`

export const DefaultButton = styled(MainButton)`
  border-color: #555;
  color: #555;
  padding-left: 12px;
  padding-right: 12px;
  &:hover {
    background-color: #555;
    color: #FFF;
    border-color: #555;
  }
  &:focus {
    background-color: #3A3A3A;
    color: #FFF;
    border-color: #3A3A3A;
  }
  &:disabled {
    background-color: #FFF;
    color: #E7E7E7;
    border-color: #E7E7E7;
  }
`

export const DangerButton = styled(MainButton)`
  color: ${props => props.theme.colors.red.default};
  border-color: ${props => props.theme.colors.red.default};
  &:hover {
    background-color: ${props => (props.theme.colors.red.default)};
    color: #FFF;
    border-color: ${props => (props.theme.colors.red.default)};
  }
  &:focus {
    background-color: ${props => (props.theme.colors.red.focus)};
    color: #FFF;
    border-color: ${props => (props.theme.colors.red.focus)};
  }
  &:disabled {
    background-color: ${props => (props.theme.colors.red.disabled)};
    color: #FFF;
    border-color: ${props => (props.theme.colors.red.disabled)};
  }
`

export const SecondaryButton = styled(MainButton)`
  border-color: #3A559F;
  color: #3A559F;
  &:hover {
    background-color: #3A559F;
    color: #FFF;
    border-color: #3A559F;
  }
  &:focus {
    background-color: #3A559F;
    color: #FFF;
    border-color: #3A559F;
  }
  &:disabled {
    background-color: #FFF;
    color: #E7E7E7;
    border-color: #E7E7E7;
  }
`

export const SuccessButton = styled(MainButton)`
  color: ${props => props.theme.colors.green.default};
  border-color: ${props => props.theme.colors.green.default};
  &:hover {
    background-color: ${props => (props.theme.colors.green.default)};
    color: #FFF;
    border-color: ${props => (props.theme.colors.green.default)};
  }
  &:focus {
    background-color: ${props => (props.theme.colors.green.focus)};
    color: #FFF;
    border-color: ${props => (props.theme.colors.green.focus)};
  }
  &:disabled {
    background-color: #FFF;
    color: ${props => (props.theme.colors.green.disabled)};
    border-color: ${props => (props.theme.colors.green.disabled)};
  }
`

export const TabButton = styled.a`
  display: flex;
  background-color: ${props => (props.active ? '#FFF' : '#F5F5F5')};
  border: none;
  height: 35px;
  color: ${props => (props.active ? '#1C3A6F' : '#555')};
  text-align: center;
  font-size: 10px;
  text-transform: uppercase;
  font-weight: bold;
  justify-content: center;
  align-items: center;
  &:link {
    text-decoration: none;
  }
  &:hover {
    color: #1C3A6F;
  }
`

const SSOButton = styled.button`
  display: flex;
  align-items: center;
  width: ${props => (props.block ? '100%' : '196px')};
  height: 30px;
  border: none;
  box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.3);
  border-radius: 4px;
  font-size: 12px;
  transition: background .1s linear;
`

const FacebookButtonContainer = styled(SSOButton)`
  background-color: #475993;
  color: #FFF;
  padding-left: 12px;

  &:hover {
    background-color: #33447D;
  }
`

const GoogleButtonContainer = styled(SSOButton)`
  background-color: #518EF8;
  color: #FFF;
  padding-left: 3px;

  &:hover {
    background-color: #3F74D0;
  }
`

export const OutlinedButton = styled.button`
  background-color: #FFF;
  border: 1px solid ${props => props.theme.colors.darkgrey.default};
  border-radius: 3px;
  height: 46px;
  padding: 0 26px;
  font-weight: bold;
  font-size: 18px;
  color: ${props => props.theme.colors.darkgrey.default};
  transition: background .1s linear, color .1s linear;
  &:hover {
    background-color: ${props => props.theme.colors.darkgrey.default};
    color: #FFF;
  }
`

export const LinkButton = styled(OutlinedButton)`
  background: transparent;
  border: none;
  &:hover {
    background: transparent;
    color: ${props => props.theme.colors.darkgrey.default};
    text-decoration: underline;
  }
`

export const LoginButton = props => (
  <MainButton {...props} type='submit'>
    Log in
  </MainButton>
)

export const RegisterButton = props => (
  <MainButton {...props} type='submit'>
    Create Account
  </MainButton>
)

export const FacebookButton = ({ children, ...props }) => (
  <FacebookButtonContainer {...props}>
    <img src='/static/icon/facebook-icon.png' alt='facebook login' />
    <div className='w-100'>
      {children}
    </div>
  </FacebookButtonContainer>
)

export const GoogleButton = ({ children, ...props }) => (
  <GoogleButtonContainer {...props}>
    <img src='/static/icon/google-sso-icon.png' alt='google login' />
    <div className='w-100'>
      {children}
    </div>
  </GoogleButtonContainer>
)

export const OutlinedSignUpButton = styled(OutlinedButton)`
  color: #FFF;
  border-color: #FFF;

  &:hover {
    color: #7CC9EB;
    border-color: #7CC9EB;
    background-color: #FFF;
  }
`
