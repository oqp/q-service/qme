import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCogs } from '@fortawesome/free-solid-svg-icons'

import { DefaultButton } from './button'

import { queueStatusCalculator } from '../util'

export const Table = styled.table`
  border: 0px;
  color: #555;
  width: 100%;
  & thead th {
    padding-bottom: 15px;
  }

  & td {
    padding: 14px 0;
  }
  & thead > tr {
    border-bottom: 1px solid #555;
  }
  & tr {
    border-bottom: 1px solid #D0D0D0;
  }

  & td:last-child {
    padding-right: 63px;
  }

  @media (max-width: 767.98px) {
    & td:last-child {
      padding-right: 0;
    }
  }
`

export class WaitingRoomList extends React.Component {
  render() {
    const { waitingRoomList } = this.props
    return (
      <Table>
        <thead>
          <tr>
            <th>Waiting Rooms</th>
            <th className='d-none d-sm-table-cell'>Website</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {
            waitingRoomList.map(({
              id, name, description, website,
            }) => (
              <tr key={id}>
                <td>
                  <div>
                    {name}
                  </div>
                  <div style={{ fontSize: '14px' }}>
                    {description}
                  </div>
                </td>
                <td className='d-none d-sm-table-cell'>
                  {website}
                </td>
                <td className='text-right'>
                  <a href={`/panel/waiting-room/${id}/edit`}>
                    <DefaultButton className='mr-2'>
                      <FontAwesomeIcon icon={faCogs} />
                      <span className='sr-only'>Setting</span>
                    </DefaultButton>
                  </a>
                  <a href={`/panel/waiting-room/${id}`}>
                    <DefaultButton>View Events</DefaultButton>
                  </a>
                </td>
              </tr>
            ))
          }
        </tbody>
      </Table>
    )
  }
}
WaitingRoomList.propTypes = {
  waitingRoomList: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export class EventList extends React.Component {
  render() {
    const { eventList } = this.props
    return (
      <Table>
        <thead>
          <tr>
            <th style={{ width: '40%' }}>Name</th>
            <th>Subdomain</th>
            <th>Event State</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {
            eventList.map(({
              id, name, isActive, subdomain, waitingRoomId, ...event
            }) => (
              <tr key={id}>
                <td>
                  {name}
                </td>
                <td>
                  {subdomain}
                </td>
                <td>
                  {queueStatusCalculator({ ...event, isActive })}
                </td>
                <td className='text-right'>
                  <a href={`/panel/waiting-room/${waitingRoomId}/event/${id}/setting/general`}>
                    <DefaultButton className='mr-2'>
                      <FontAwesomeIcon icon={faCogs} />
                      <span className='sr-only'>Setting</span>
                    </DefaultButton>
                  </a>
                  <a href={`/panel/waiting-room/${waitingRoomId}/event/${id}/dashboard`}>
                    <DefaultButton>Monitor</DefaultButton>
                  </a>
                </td>
              </tr>
            ))
          }
        </tbody>
      </Table>
    )
  }
}
EventList.propTypes = {
  eventList: PropTypes.arrayOf(PropTypes.object).isRequired,
}
