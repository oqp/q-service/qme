import styled from 'styled-components'

export const PageTitle = styled.h1`
  font-size: 25px;
  color: #000;
  font-weight: bold;
  margin-bottom: 23px;
`

export const LoginFormTitle = styled.h1`
  font-size: 35px;
  color: #555;
  font-weight: bold;
`

export const Heading3 = styled.h3`
  font-size: 20px;
  color: #555;
  font-weight: bold;
`
