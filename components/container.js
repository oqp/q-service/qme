import styled from 'styled-components'
import PropTypes from 'prop-types'
import { EventCard } from './card'

export const LoginFormContainer = styled.div`
  height: calc(100% - 35px);
  background-color: #FFF;
  @media (min-width: 768px) {
    box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.25);
    padding: 32px 40px;
    /* height: 579px; */
    width: 100%;
  }
`

export const LoginPageContainer = styled.div`
  height: 100vh;
  @media (min-width: 768px) {
    background-image: url('/static/image/waitingline-bg.png');
    background-attachment: fixed;
    background-size: contain;
    background-position: center 93px;
    background-repeat: no-repeat;
  }
`
const EventListContainerHeader = styled.div`
  background: #F4F4F4;
  border-radius: 3px 3px 0px 0px;
  color: #555;
  font-weight: bold;
  font-size: 20px;
  padding: 9px 17px;
`

const EventListContainerBody = styled.div`
  background-color: #FFF;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
  border-radius: 3px;
  padding: 29px 40px;
  display: flex;
  flex-wrap: wrap;
`

export const EventListContainer = ({ events }) => (
  <div>
    <EventListContainerHeader>Events</EventListContainerHeader>
    <EventListContainerBody>
      {!events && (
        <div className='col-12 text-center'>Error</div>
      )}
      {events && events.map(event => (
        <EventCard key={event.id} {...event} />
      ))}
    </EventListContainerBody>
  </div>
)
EventListContainer.defaultProps = {
  events: null,
}
EventListContainer.propTypes = {
  events: PropTypes.arrayOf(PropTypes.object),
}

export const FormContainer = styled.div`
  & .form-section {
    padding: 0 113px;

    @media (max-width: 767.98px) {
      padding: 0 50px;
    }
    @media (max-width: 575.98px) {
      padding: 0 25px;
    }
  }

  & .form-footer {
    text-align: right;
    padding: 0 43px;
    margin-top: 107px;

    @media (max-width: 575.98px) {
      text-align: center;
      padding: 0 25px;
      margin-top: 45px;
    }
  }
`
