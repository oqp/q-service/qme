import styled from 'styled-components'
import { Input } from 'reactstrap'
import { Field } from 'react-final-form'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import DatePicker from 'react-datepicker'

import 'react-datepicker/dist/react-datepicker.css'
import moment from 'moment'

const DefaultTextFieldStyled = styled(Input)`
  background: #FFF;
  border: 1px solid #D0D0D0;
  box-sizing: border-box;
  border-radius: 3px;
  height: 28px;
  font-size: 12px;
  transition: border .2s, box-shadow .2s;
  &::placeholder {
    color: #C0C0C0;
  }
  &:focus {
    border: 0.5px solid #518EF8;
    box-shadow: 0px 0px 3px #518EF8;
    border-radius: 3px;
  }

  &.is-invalid {
    border: 1px solid #DC3545;
    border-radius: 3px;
  }

  &.is-invalid:focus {
    border: 1px solid #DC3545;
    box-shadow: 0px 0px 3px #DC3545;
    border-radius: 3px;
  }
`

const FloatingLabelWrapper = styled.div`
  width: 100%;
  input::placeholder {
    color: transparent;
  }
  input:focus, input:not(:placeholder-shown) {
    & + label {
      top: -1.6rem;
      left: .5rem;
    }
    &::placeholder {
      color: #C0C0C0;
    }
  }
  label {
    transition: all .2s;
    user-select: none;
    display: initial;
    line-height: 1;
    padding: .9em;
    position: absolute;
    top: -.2rem;
    left: inherit;
    font-size: 12px;
  }
`
export const FloatingLabelTextField = ({
  className, id, label, style, ...props
}) => (
  <FloatingLabelWrapper style={style}>
    <DefaultTextFieldStyled
      {...props}
      className={className}
      label={label}
      id={id}
    />
    <label htmlFor={id}>{label}</label>
  </FloatingLabelWrapper>
)

const Label = styled.label`
  font-weight: bold;
  text-align: left;
  padding: 0;
`

export const MyInput = styled.input`
  background-color: orange;
`

const RequirementText = styled.div`
  font-size: 10px;
  font-weight: normal;
  line-height: 0.5;
  letter-spacing: 0.04rem;
  color: #909090;
`

const HelpText = styled.div`
  font-size: 10px;
  margin-top: 5px;
  text-align: left;
  letter-spacing: 0.04rem;
  color: #909090;
  line-height: 14px;
`

const ErrorText = styled.div`
  width: 100%;
  margin-top: .25rem;
  font-size: 10px;
  color: #dc3545;
  text-align: left;
  letter-spacing: 0.04rem;
`

export const MyInput2 = () => <input style={{ backgroundColor: 'orange' }} />

export const DefaultTextField = props => (
  <Field {...props}>
    {fieldProps => (
      <>
        <FloatingLabelTextField
          {...props}
          {...fieldProps.input}
          className={classNames('form-control', (((!fieldProps.meta.dirtySinceLastSubmit && fieldProps.meta.submitError)) || fieldProps.meta.error) && fieldProps.meta.touched ? 'is-invalid' : undefined)}
        />
        {(fieldProps.meta.error || (!fieldProps.meta.dirtySinceLastSubmit && fieldProps.meta.submitError)) && fieldProps.meta.touched && (
          <ErrorText>
            {(fieldProps.meta.dirty && fieldProps.meta.submitError) || fieldProps.meta.error}
          </ErrorText>
        )}
      </>
    )}
  </Field>
)

export const InlineLabelTextField = props => (
  <Field {...props}>
    {fieldProps => (
      <div className='form-group row'>
        <Label htmlFor={props.id} className='col-12 col-sm-3 col-form-label'>
          <div>
            {props.label}
          </div>
          { props.showRequired
            && (
            <RequirementText>
              {'('}
              required
              {')'}
            </RequirementText>
            )
          }
        </Label>
        <div className='col-12 col-sm-9 d-flex flex-column align-items-start'>
          <div className='w-100 d-flex align-items-center'>
            <DefaultTextFieldStyled
              {...props}
              {...fieldProps.input}
              className={classNames('form-control', (fieldProps.meta.error || (!fieldProps.meta.dirtySinceLastSubmit && fieldProps.meta.submitError)) && fieldProps.meta.touched ? 'is-invalid' : undefined)}
            />
            {props.appendText && (
              <span style={{ fontSize: '12px', marginLeft: '12px' }}>{props.appendText}</span>
            )}
          </div>
          {(fieldProps.meta.error || (!fieldProps.meta.dirtySinceLastSubmit && fieldProps.meta.submitError)) && fieldProps.meta.touched && (
            <ErrorText>
              {(fieldProps.meta.dirty && fieldProps.meta.submitError) || fieldProps.meta.error}
            </ErrorText>
          )}
          {props.helpText && (
            <HelpText>{props.helpText}</HelpText>
          )}
        </div>
      </div>
    )}
  </Field>
)

InlineLabelTextField.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
}

export const CheckboxStyle = styled.input`
  background: #FFFFFF;
  box-shadow: inset 0px 0px 2px rgba(0, 0, 0, 0.25);
  border-radius: 1px;
  width: 10px;
  height: 10px;
`

export const Checkbox = props => (
  <Field {...props} type='checkbox'>
    {fieldProps => (
      <CheckboxStyle {...props} {...fieldProps.input} />
    )}
  </Field>
)

export const SidebarInput = styled.input`
  width: 190px;
  height: 33px;
  background: #FFFFFF;
  border: 1px solid #FCAC18;
  border-radius: 3px;
  margin-bottom: 16px;
`

export const InlineLabelTextArea = props => (
  <Field {...props} component='textarea'>
    {fieldProps => (
      <div className='form-group row'>
        <Label htmlFor={props.id} className='col-12 col-sm-3 col-form-label'>
          <div>
            {props.label}
          </div>
          { props.showRequired
            && (
            <RequirementText>
              {'('}
              required
              {')'}
            </RequirementText>
            )
          }
        </Label>
        <div className='col-12 col-sm-9 d-flex flex-column align-items-start'>
          <DefaultTextFieldStyled {...props} {...fieldProps.input} type='textarea' className='form-control' rows='4' />
          <HelpText>{props.helpText}</HelpText>
        </div>
      </div>
    )}
  </Field>
)
InlineLabelTextArea.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
}

export const InlineLabelDateTimePicker = props => (
  <Field {...props}>
    {fieldProps => (
      <div className='form-group row'>
        <Label htmlFor={props.id} className='col-12 col-sm-3 col-form-label'>
          <div>
            {props.label}
          </div>
          { props.showRequired
            && (
            <RequirementText>
              {'('}
              required
              {')'}
            </RequirementText>
            )
          }
        </Label>
        <div className='col-12 col-sm-9 d-flex flex-column align-items-start'>
          <div>
            <DefaultTextFieldStyled
              as={DatePicker}
              {...props}
              {...fieldProps.input}
              className={classNames('form-control', (((!fieldProps.meta.dirtySinceLastSubmit && fieldProps.meta.submitError)) || fieldProps.meta.error) && fieldProps.meta.touched ? 'is-invalid' : undefined)}
              onChange={value => fieldProps.input.onChange(value)}
              selected={(fieldProps.input.value)}
              showTimeSelect
              dateFormat='dd/MM/yyyy h:mm aa'
              autoComplete='off'
            />
            {/* {props.appendText && ( */}
            <span style={{ fontSize: '12px', marginLeft: '12px' }}>{`(GMT${moment().format('Z')})`}</span>
            {/* )} */}
          </div>
          {(fieldProps.meta.error || (!fieldProps.meta.dirtySinceLastSubmit && fieldProps.meta.submitError)) && fieldProps.meta.touched && (
            <ErrorText>
              {(fieldProps.meta.dirty && fieldProps.meta.submitError) || fieldProps.meta.error}
            </ErrorText>
          )}
          {props.helpText && (
            <HelpText>{props.helpText}</HelpText>
          )}
        </div>
      </div>
    )}
  </Field>
)

InlineLabelDateTimePicker.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
}

export const ErrorMessage = ({ name }) => (
  <Field
    name={name}
    subscription={{ touched: true, error: true }}
    render={({ meta: { touched, error } }) => (touched && error ? (
      <ErrorText>
        {error}
      </ErrorText>
    ) : null)
    }
  />
)
