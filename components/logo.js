import styled from 'styled-components'

export const Logo = styled.img`
  width: 51px;
  @media (min-width: 768px) {
    width: 88px;
  }
`
