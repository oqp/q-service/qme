import React from 'react'
import { Form, FormSpy } from 'react-final-form'
import getConfig from 'next/config'

import { InlineLabelTextField, InlineLabelDateTimePicker } from '../../input'
import { SuccessButton, DangerButton } from '../../button'
import { FormContainer } from '../../container'

import EventService from '../../../services/event'
import formValidation from '../../../config/formValidation'
import { isEventFieldEditable } from '../../../util'

const { publicRuntimeConfig } = getConfig()

export default class EditEventForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isPristine: null,
      isSubmitting: null,
    }
  }

  componentDidMount() {
    window.onbeforeunload = (e) => {
      const { isPristine, isSubmitting } = this.state
      if (isPristine === false && isSubmitting === false) {
        const message = 'Changes you made may not be saved'
        const event = (e || window.event)
        event.returnValue = message
        return message
      }
      return undefined
    }
  }

  async submit({
    id, name, openTime, closeTime, maxOutflowAmount, redirectURL, sessionTime, arrivalTime, isActive, subdomain,
  }) {
    const { event: { waitingRoomId } } = this.props
    const result = await EventService.editEvent(id, name, openTime, closeTime, maxOutflowAmount, redirectURL, sessionTime, arrivalTime, isActive, subdomain, waitingRoomId)
    return result
  }

  cancelEdit() {
    const { event: { waitingRoomId } } = this.props
    window.location = `/panel/waiting-room/${waitingRoomId}`
  }

  render() {
    const { event } = this.props
    if (event) {
      if (event.openTime) {
        event.openTime = new Date(event.openTime)
      }
      if (event.closeTime) {
        event.closeTime = new Date(event.closeTime)
      }
      event.sessionTime /= 60
      event.arrivalTime /= 60
    }
    return (
      <FormContainer>
        <div className='form-section'>
          <div style={{ fontSize: '20px', marginBottom: '17px' }}>Event Settings</div>
          {event && !isEventFieldEditable(event) && (
            <div style={{ fontSize: '12px', fontWeight: 'bold' }} className='text-center mb-3'>
              *There are some disabled fields because the event is ALREADY started.
            </div>
          )}
          <Form
            onSubmit={values => this.submit({ ...values })}
            validate={formValidation.eventForm}
            initialValues={event}
            render={({
              handleSubmit,
              submitError,
              pristine,
              submitting,
            }) => (
              <form onSubmit={handleSubmit}>
                <div>
                  {submitError}
                </div>
                <div>
                  <InlineLabelTextField
                    label='Name'
                    name='name'
                    id='name'
                    helpText={'The name of your event. This will also display to the queue audience. *Note: After the queue is open you WON\'T be able to edit this field'}
                    showRequired
                    disabled={event && !isEventFieldEditable(event)}
                  />
                  <InlineLabelDateTimePicker
                    label='Open time'
                    name='openTime'
                    id='openTime'
                    helpText={'The time that queue will open for audience to get in line. *Note: After the queue is open you WON\'T be able to edit this field'}
                    showRequired
                    disabled={event && !isEventFieldEditable(event)}
                  />
                  <InlineLabelDateTimePicker
                    label='Close time'
                    name='closeTime'
                    id='closeTime'
                    helpText={'The time that queue will close. After the queue is close, new queue number won\'t be given and queue will still be running until there is no remaining audience waiting in line'}
                    showRequired
                  />
                  <InlineLabelTextField
                    label='Max Outflow'
                    name='maxOutflowAmount'
                    id='maxOutflowAmount'
                    helpText='The maximum number of audience that the desitnation website would call from the queue'
                    showRequired
                    appendText='users'
                    type='number'
                  />
                  <InlineLabelTextField
                    label='Redirect URL'
                    name='redirectURL'
                    id='redirectURL'
                    helpText={'The URL to send the audience to when their queue is arrived *Note: After the queue is open you won\'t be able to edit this field'}
                    showRequired
                    disabled={event && !isEventFieldEditable(event)}
                  />
                  <InlineLabelTextField
                    label='Session Time'
                    name='sessionTime'
                    id='sessionTime'
                    helpText='This is the time limit for each audience when they are being served on the destination website. After the time is up, the audience will be sent to get a new queue number(only for integrated website) and next audience will be called'
                    showRequired
                    appendText='minutes'
                    type='number'
                  />
                  <InlineLabelTextField
                    label='Arrival Time'
                    name='arrivalTime'
                    id='arrivalTime'
                    helpText='This is how long the queue should wait for an audience to get to the destination website. After the time is up the After the time is up, the audience will be sent to get a new queue number(only for integrated website) and next audience will be called'
                    showRequired
                    appendText='minutes'
                    type='number'
                    placeholder='Default: 5'
                  />
                  <InlineLabelTextField
                    label='Subdomain'
                    id='subdomain'
                    name='subdomain'
                    showRequired
                    helpText={'This is your queue web address. *Note: After the queue is open you won\'t be able to edit this field'}
                    appendText={`.${publicRuntimeConfig.QCLERK_HOSTNAME}`}
                    disabled={event && !isEventFieldEditable(event)}
                  />
                </div>
                <div className='float-right'>
                  <DangerButton style={{ padding: '0 13px' }} type='button' onClick={() => this.cancelEdit()} className='mr-4'>Cancel</DangerButton>
                  <SuccessButton type='submit' disabled={pristine || submitting} style={{ padding: '0 13px' }}>Save</SuccessButton>
                </div>
                <FormSpy
                  subscription={{ pristine: true, submitting: true }}
                  onChange={(props) => {
                    this.setState({ isPristine: props.pristine, isSubmitting: props.submitting })
                  }}
                />
              </form>
            )}
          />
        </div>
      </FormContainer>
    )
  }
}
