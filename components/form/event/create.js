import React from 'react'
import { Form } from 'react-final-form'
import Swal from 'sweetalert2'
import getConfig from 'next/config'

import { InlineLabelTextField, InlineLabelDateTimePicker } from '../../input'
import { MainButton, DangerButton } from '../../button'

import EventService from '../../../services/event'
import formValidation from '../../../config/formValidation'

const { publicRuntimeConfig } = getConfig()

export default class CreateEventForm extends React.Component {
  async submit({
    name, openTime, closeTime, maxOutflowAmount, redirectURL, sessionTime, arrivalTime, subdomain, isActive = false,
  }) {
    const { waitingRoomId } = this.props
    const result = await EventService.createEvent(
      name, openTime, closeTime, maxOutflowAmount, redirectURL, sessionTime, arrivalTime, isActive, subdomain, waitingRoomId,
    )
    return result
  }

  cancelEdit() {
    Swal.close()
  }

  render() {
    return (
      <div>
        <Form
          onSubmit={values => this.submit({ ...values })}
          validate={formValidation.eventForm}
          initialValues={{
            arrivalTime: '5',
          }}
          render={({
            handleSubmit,
            submitError,
            pristine,
            submitting,
          }) => (
            <form onSubmit={handleSubmit}>
              <div>
                {submitError}
              </div>
              <div className='form-section'>
                <InlineLabelTextField
                  label='Name'
                  name='name'
                  id='name'
                  helpText={'The name of your event. This will also display to the queue audience. *Note: After the queue is open you WON\'T be able to edit this field'}
                  showRequired
                />
                <InlineLabelDateTimePicker
                  label='Open time'
                  name='openTime'
                  id='openTime'
                  helpText={'The time that queue will open for audience to get in line. *Note: After the queue is open you WON\'T be able to edit this field'}
                  showRequired
                />
                <InlineLabelDateTimePicker
                  label='Close time'
                  name='closeTime'
                  id='closeTime'
                  helpText={'The time that queue will close. After the queue is close, new queue number won\'t be given and queue will still be running until there is no remaining audience waiting in line'}
                  showRequired
                />
                <InlineLabelTextField
                  label='Max Outflow'
                  name='maxOutflowAmount'
                  id='maxOutflowAmount'
                  helpText='The maximum number of audience that the desitnation website would call from the queue'
                  showRequired
                  appendText='users'
                  type='number'
                />
                <InlineLabelTextField
                  label='Redirect URL'
                  name='redirectURL'
                  id='redirectURL'
                  helpText={'The URL to send the audience to when their queue is arrived *Note: After the queue is open you won\'t be able to edit this field'}
                  showRequired
                />
                <InlineLabelTextField
                  label='Session Time'
                  name='sessionTime'
                  id='sessionTime'
                  helpText='This is the time limit for each audience when they are being served on the destination website. After the time is up, the audience will be sent to get a new queue number(only for integrated website) and next audience will be called'
                  showRequired
                  appendText='minutes'
                  type='number'
                />
                <InlineLabelTextField
                  label='Arrival Time'
                  name='arrivalTime'
                  id='arrivalTime'
                  helpText='This is how long the queue should wait for an audience to get to the destination website. After the time is up the After the time is up, the audience will be sent to get a new queue number(only for integrated website) and next audience will be called'
                  showRequired
                  appendText='minutes'
                  type='number'
                  placeholder='Default: 5'
                />
                <InlineLabelTextField
                  label='Subdomain'
                  id='subdomain'
                  name='subdomain'
                  showRequired
                  helpText={'This is your queue web address. *Note: After the queue is open you won\'t be able to edit this field'}
                  appendText={`.${publicRuntimeConfig.QCLERK_HOSTNAME}`}
                />
              </div>
              <div className='form-footer'>
                <DangerButton style={{ padding: '0 13px' }} type='button' onClick={() => this.cancelEdit()} className='mr-4'>Cancel</DangerButton>
                <MainButton type='submit' disabled={pristine || submitting} style={{ padding: '0 13px' }}>Create</MainButton>
              </div>
            </form>
          )}
        />
      </div>
    )
  }
}
