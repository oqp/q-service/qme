import React from 'react'
import { Form } from 'react-final-form'
import styled, { ThemeProvider } from 'styled-components'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCopy } from '@fortawesome/free-solid-svg-icons'


import { DefaultTextField } from '../../input'
import { FormContainer } from '../../container'
import {
  SuccessButton, DangerButton, DefaultButton, MainButton,
} from '../../button'

import defaultTheme from '../../../theme/default'

import EventService from '../../../services/event'

const randomString = (length) => {
  let result = ''
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

const hiddenToken = randomString(12)
const WarningText = styled.div`
  color: ${props => props.theme.colors.red.default};
  font-size: 10px;
`

export default class IntegrationEventForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isNewGenerate: false,
      token: null,
    }
  }

  copyWaitingRoomUUIDToClipboard() {
    this.copyToClipboard('waitingRoomUUID')
  }

  copyEventUUIDToClipboard() {
    this.copyToClipboard('eventUUID')
  }

  copyTokenToClipboard() {
    this.copyToClipboard('integrationToken')
  }

  copyToClipboard(elementId) {
    const copyText = document.getElementById(elementId)
    copyText.select()
    copyText.setSelectionRange(0, 99999) /* For mobile devices */
    document.execCommand('copy')
    Swal.fire({
      text: 'Text copied',
      toast: true,
      showConfirmButton: false,
      position: 'top-right',
      type: 'success',
      timer: 5000,
    })
  }

  async generateToken() {
    const { event } = this.props
    const { token } = this.state
    if ((event && event.integrateToken) || token) {
      const confirmGenerate = await withReactContent(Swal).fire({
        type: 'warning',
        showConfirmButton: false,
        html: (
          <ThemeProvider theme={defaultTheme}>
            <div>
              <div className='text-center' style={{ fontSize: '20px' }}>
                Are you sure?
              </div>
              <div className='text-center' style={{ fontSize: '16px', marginTop: '20px' }}>
                You have already generate token. If you generate token again, it will cancel the previous token.
              </div>
              <div style={{ marginTop: '43px' }}>
                <DangerButton
                  className='mr-4'
                  onClick={() => Swal.clickCancel()}
                >
                  Cancel
                </DangerButton>
                <SuccessButton
                  onClick={() => Swal.clickConfirm()}
                >
                  Confirm
                </SuccessButton>
              </div>
            </div>
          </ThemeProvider>
        ),
      })
      if (confirmGenerate.value) {
        this.generateNewToken()
      }
    } else {
      this.generateNewToken()
    }
  }

  async generateNewToken() {
    const { event: { id, waitingRoomId } } = this.props
    const newToken = await EventService.generateToken(id, waitingRoomId)
    this.setState({
      isNewGenerate: true,
      token: newToken,
    })
    return newToken
  }

  render() {
    const { isNewGenerate, token } = this.state
    const { event, waitingRoom } = this.props
    return (
      <FormContainer>
        <div className='form-section'>
          <div style={{ fontSize: '20px' }}>Integrate with your Website</div>
          <div style={{
            fontSize: '16px', marginTop: '17px', color: '#555', marginBottom: '32px',
          }}
          >
            For more information,
            {' '}
            <a target='_blank' href='/static/document/oqp-destination-website-integration.pdf'>click here</a>
          </div>
          <div className='mt-3'>
            <Form
              onSubmit={() => null}
              initialValues={{
                integrationToken: token || (event && event.integrateToken === 'generated' ? hiddenToken : null),
                waitingRoomUUID: waitingRoom && waitingRoom.uuid,
                eventUUID: event && event.uuid,
              }}
              render={({ submitError }) => (
                <form>
                  <div>
                    <span>
                      Waiting room UUID:&nbsp;
                    </span>
                    <div className='input-group'>
                      <DefaultTextField
                        id='waitingRoomUUID'
                        name='waitingRoomUUID'
                        readOnly
                        style={{ width: '70%' }}
                      />
                      <div className='input-group-append'>
                        <DefaultButton type='button' onClick={() => this.copyWaitingRoomUUIDToClipboard()}>
                          <FontAwesomeIcon icon={faCopy} />
                        </DefaultButton>
                      </div>
                    </div>
                  </div>
                  <div className='mt-3'>
                    <span>
                      Event UUID:&nbsp;
                    </span>
                    <div className='input-group'>
                      <DefaultTextField
                        id='eventUUID'
                        name='eventUUID'
                        readOnly
                        style={{ width: '70%' }}
                      />
                      <div className='input-group-append'>
                        <DefaultButton type='button' onClick={() => this.copyEventUUIDToClipboard()}>
                          <FontAwesomeIcon icon={faCopy} />
                        </DefaultButton>
                      </div>
                    </div>
                  </div>
                  <hr />
                  <div>
                    <span>
                      Integrate token:&nbsp;
                    </span>
                    <div className='input-group'>
                      <DefaultTextField
                        name='integrationToken'
                        id='integrationToken'
                        readOnly
                        type={`${isNewGenerate ? 'text' : 'password'}`}
                        label=''
                        placeholder=''
                        style={{ width: '70%' }}
                      />
                      <div className='input-group-append'>
                        <DefaultButton type='button' onClick={() => this.copyTokenToClipboard()} disabled={!isNewGenerate}>
                          <FontAwesomeIcon icon={faCopy} />
                        </DefaultButton>
                      </div>
                    </div>
                  </div>
                  {isNewGenerate && (
                    <WarningText>Make sure you save it - you won’t be able to see it again.</WarningText>
                  )}
                  <div />
                  <div className='mt-4'>
                    <MainButton type='button' onClick={() => this.generateToken()}>Generate token</MainButton>
                  </div>
                  {submitError}
                </form>
              )}
            />
          </div>
        </div>
      </FormContainer>
    )
  }
}
