import React from 'react'
import { Form, FormSpy } from 'react-final-form'

import { InlineLabelTextField, InlineLabelTextArea } from '../../input'
import { DangerButton, SuccessButton } from '../../button'

import WaitingRoomService from '../../../services/waitingRoom'
import formValidation from '../../../config/formValidation'
import { FormContainer } from '../../container'

export default class EditWaitingRoomForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isPristine: null,
    }
  }

  componentDidMount() {
    window.onbeforeunload = (e) => {
      const { isPristine, isSubmitting } = this.state
      if (isPristine === false && isSubmitting === false) {
        const message = 'Changes you made may not be saved'
        const event = (e || window.event)
        event.returnValue = message
        return message
      }
      return undefined
    }
  }

  async submit({
    id, name, description, website,
  }) {
    const result = await WaitingRoomService.editWaitingRoom(id, name, description, website)
    return result
  }

  cancelEdit() {
    window.location = '/panel/waiting-room'
  }

  render() {
    const { waitingRoom } = this.props
    return (
      <FormContainer>
        <div>
          <div className='form-section'>
            <div style={{ fontSize: '20px', marginBottom: '17px' }}>Waiting room Settings</div>
            <Form
              onSubmit={values => this.submit(values)}
              validate={formValidation.createWaitingRoomForm}
              initialValues={waitingRoom}
              render={({
                handleSubmit, submitting, submitError, pristine,
              }) => (
                <form onSubmit={handleSubmit}>
                  {submitError && (
                    <div>
                      {submitError}
                    </div>
                  )}
                  <div>
                    <input type='hidden' name='id' id='id' />
                    <InlineLabelTextField
                      label='Name'
                      id='name'
                      name='name'
                      showRequired
                      helpText='Name of the collection, this will display only in your waiting room lists.'
                      autoFocus
                    />
                    <InlineLabelTextArea
                      label='Description'
                      id='description'
                      name='description'
                      helpText='A briefly description to remind yourself what this collection is for'
                    />
                    <InlineLabelTextField
                      label='Website'
                      id='website'
                      name='website'
                      showRequired
                      helpText='Domain of a website you wish to use with our platform'
                    />
                  </div>
                  <div className='text-right' style={{ padding: '0 43px', marginTop: '107px' }}>
                    <DangerButton style={{ padding: '0 13px' }} type='button' onClick={() => this.cancelEdit()} className='mr-4'>Cancel</DangerButton>
                    <SuccessButton type='submit' disabled={pristine || submitting} style={{ padding: '0 13px' }}>Save</SuccessButton>
                  </div>
                  <FormSpy
                    subscription={{ pristine: true, submitting: true }}
                    onChange={(props) => {
                      this.setState({ isPristine: props.pristine, isSubmitting: props.submitting })
                    }}
                  />
                </form>
              )}
            />
          </div>
        </div>
      </FormContainer>
    )
  }
}
