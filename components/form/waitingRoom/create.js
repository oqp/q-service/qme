import React from 'react'
import { Form } from 'react-final-form'
import Swal from 'sweetalert2'

import { InlineLabelTextField, InlineLabelTextArea } from '../../input'
import { MainButton, DangerButton } from '../../button'

import WaitingRoomService from '../../../services/waitingRoom'
import formValidation from '../../../config/formValidation'

export default class CreateWaitingRoomForm extends React.Component {
  async submit({
    name, description, website,
  }) {
    const result = await WaitingRoomService.createWaitingRoom(name, description, website)
    return result
  }

  cancelCreate() {
    Swal.close()
  }

  render() {
    return (
      <Form
        onSubmit={values => this.submit(values)}
        validate={formValidation.createWaitingRoomForm}
        render={({
          handleSubmit, submitting, submitError, pristine,
        }) => (
          <form onSubmit={handleSubmit}>
            {submitError && (
              <div>
                {submitError}
              </div>
            )}
            <div className='form-section'>
              <InlineLabelTextField
                label='Name'
                id='name'
                name='name'
                showRequired
                helpText='Name of the collection, this will display only in your waiting room lists.'
              />
              <InlineLabelTextArea
                label='Description'
                id='description'
                name='description'
                helpText='A briefly description to remind yourself what this collection is for'
              />
              <InlineLabelTextField
                label='Website'
                id='website'
                name='website'
                showRequired
                helpText='Domain of a website you wish to use with our platform'
              />
            </div>
            <div className='form-footer'>
              <DangerButton style={{ padding: '0 13px' }} type='button' onClick={() => this.cancelCreate()} className='mr-4'>Cancel</DangerButton>
              <MainButton type='submit' disabled={pristine || submitting} style={{ padding: '0 13px' }}>Create</MainButton>
            </div>
          </form>
        )}
      />
    )
  }
}
