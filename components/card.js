import styled from 'styled-components'
import { DefaultButton } from './button'

const WaitingRoomCardStyle = styled.a`
  display: block;
  width: 288px;
  height: 102px;
  background: #FFF;
  color: #000;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
  border-radius: 3px;
  padding: 13px 15px;
  margin-bottom: 80px;

  &:hover {
    box-shadow: 0px 0px 10px #A3D4DD;
    transition: .25s all;
    cursor: pointer;
  }

  &:link {
    text-decoration: none;
  }
`

const CardTitle = styled.div`
  font-size: 17px;
  font-weight: bold;
  margin-bottom: 10px;
`

const CardDescription = styled.div`
  font-size: 16px;
  overflow: hidden;
  position: relative; 
  line-height: 21px;
  max-height: 42px; 
  text-align: justify;  
  margin-right: -1em;
  padding-right: 1em;
  &:before {
    content: '..';
    position: absolute;
    right: 7px;
    bottom: 0;
  }
  &:after {
    content: '';
    position: absolute;
    right: 0;
    width: 1em;
    height: 1em;
    margin-top: 0.2em;
    background: white;
  }
`

export const WaitingRoomCard = ({ title, description, ...rest }) => (
  <WaitingRoomCardStyle {...rest}>
    <CardTitle>
      {title}
    </CardTitle>
    <CardDescription>
      {description}
    </CardDescription>
  </WaitingRoomCardStyle>
)

const EventTitle = styled.div`
  font-size: 16px;
  line-height: 18px;
  color: #555555;
  font-weight: bold;
  margin-bottom: 14px;
`
export const EventCard = ({
  id, name, isActive, waitingRoomId,
}) => (
  <div className='col-6 d-flex' style={{ marginBottom: '64px' }}>
    <div style={{ marginRight: '20px' }}>
      <img src='/static/icon/event-icon.png' alt='event icon' />
    </div>
    <div>
      <EventTitle>
        {name}
        {' '}
        {isActive ? '(active)' : '(inactive)'}
      </EventTitle>
      <div>
        <a href={`/panel/waiting-room/${waitingRoomId}/event/${id}/setting/general`} style={{ marginRight: '21px' }}>
          <DefaultButton>Settings</DefaultButton>
        </a>
        <a href={`/panel/waiting-room/${waitingRoomId}/event/${id}/dashboard`}>
          <DefaultButton>Monitor</DefaultButton>
        </a>
      </div>
    </div>
  </div>
)

export const DashboardCardContainer = styled.div`
  border: 1px solid #D0D0D0;
  border-radius: 3px;
  padding: 20px 25px 34px;
  display: flex;
  justify-content: start;
  align-items: flex-start;
  width: 100%;
  flex-direction: column;
  color: #555;
`
export const DashboardCardTitle = styled.div`
  font-size: 16px;
  font-weight: bold;
  color: #555;
  margin-bottom: 18px;
`

export const DashboardCard = ({ title, children, titleColor }) => (
  <DashboardCardContainer>
    <DashboardCardTitle>
      {titleColor && (
        <div style={{
          width: '10px', height: '10px', backgroundColor: titleColor, display: 'inline-block', marginRight: '5px',
        }}
        />
      )}
      {title}
    </DashboardCardTitle>
    {children}
  </DashboardCardContainer>
)

export const FormCard = styled.div`
  @media (min-width: 768px) {
    width: 526px;
    margin: 0 auto;
  }
`

export const LoginFormCardFooter = styled.div`
  margin-top: 31px;
  font-weight: bold;
  text-align: center;
  color: #555;
  font-size: 12px;
`
