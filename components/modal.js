import React from 'react'
import styled, { ThemeProvider } from 'styled-components'

import CreateEventForm from './form/event/create'
import CreateWaitingRoomForm from './form/waitingRoom/create'
import { FormContainer } from './container'

import defaultTheme from '../theme/default'

const ModalHeader = styled.div`
  padding: 0 81px;
  @media (max-width: 575.98px) {
    padding: 0 25px;
  }
`

export class CreateWaitingRoomModal extends React.Component {
  render() {
    return (
      <ThemeProvider theme={defaultTheme}>
        <div>
          <div>
            <ModalHeader>
              <div className='text-center'>Create Waiting Room</div>
              <hr style={{ borderColor: '#555' }} />
            </ModalHeader>
            <FormContainer>
              <CreateWaitingRoomForm />
            </FormContainer>
          </div>
        </div>
      </ThemeProvider>
    )
  }
}

export class CreateEventModal extends React.Component {
  render() {
    const { waitingRoomId } = this.props
    return (
      <ThemeProvider theme={defaultTheme}>
        <div>
          <div>
            <ModalHeader>
              <div className='text-center'>Create Event</div>
              <hr style={{ borderColor: '#555' }} />
            </ModalHeader>
            <FormContainer>
              <CreateEventForm waitingRoomId={waitingRoomId} />
            </FormContainer>
          </div>
        </div>
      </ThemeProvider>
    )
  }
}
