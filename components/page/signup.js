import React from 'react'

import { Form } from 'react-final-form'
import {
  TabButton, RegisterButton, GoogleButton, FacebookButton, OutlinedSignUpButton,
} from '../button'
import {
  DefaultTextField, Checkbox, ErrorMessage, FloatingLabelTextField,
} from '../input'
import { LoginFormContainer, LoginPageContainer } from '../container'
import { LoginFormTitle } from '../title'
import { Logo } from '../logo'
import { LoginFormCardFooter, FormCard } from '../card'

import logger from '../../config/logger'
import userService from '../../services/user'
import formValidation from '../../config/formValidation'

class RegisterPageComponent extends React.Component {
  async submitForm(values) {
    try {
      const result = await userService.signupWithEmail(values)
      return result
    } catch (err) {
      logger.error(err)
      return false
    }
  }

  render() {
    return (
      <LoginPageContainer>
        <div className='d-flex d-md-none'>
          <TabButton className='w-50' active href='/signup'>Sign up</TabButton>
          <TabButton className='w-50' href='/login'>Log in</TabButton>
        </div>
        <div className='pt-3 px-md-5'>
          <h6 className='text-center text-md-left my-3'>
            <a href='/'>
              <Logo src='/static/image/logo.svg' alt='Q-Me logo' />
            </a>
          </h6>
          <h6 className='d-md-none text-center mb-4 text-muted'>
            Integrate, Customize yours.
          </h6>
          <h6 className='d-md-none text-center mb-4' style={{ color: '#BBB' }}>
            Join OQP now.
          </h6>
          <FormCard className='d-md-flex justify-content-center align-items-stretch'>
            <LoginFormContainer className='px-5'>
              <LoginFormTitle className='d-none d-sm-none d-md-block mb-5'>
                Sign Up
              </LoginFormTitle>
              {/* <div className='row'>
                <div className='col-12 col-lg-6'>
                  <FacebookButton block style={{ marginBottom: '15px' }}>
                    Sign Up with Facebook
                  </FacebookButton>
                </div>
                <div className='col-12 col-lg-6'>
                  <GoogleButton block style={{ marginBottom: '15px' }}>
                    Sign Up with Google
                  </GoogleButton>
                </div>
              </div>
              <div className='d-flex justify-content-center align-items-center px-2' style={{ marginBottom: '15px' }}>
                <hr className='w-75' style={{ borderColor: '#D0D0D0' }} />
                <div className='px-2' style={{ color: '#555555' }}>
                  or
                </div>
                <hr className='w-75' style={{ borderColor: '#D0D0D0' }} />
              </div> */}
              <Form
                onSubmit={values => this.submitForm(values)}
                validate={formValidation.signupForm}
                render={({
                  handleSubmit, submitting, submitError,
                }) => (
                  <form onSubmit={handleSubmit}>
                    <div className='row'>
                      <div className='col-12 col-sm-6'>
                        <div className='form-group mb-4'>
                          <DefaultTextField
                            className='form-control'
                            type='text'
                            id='firstname'
                            name='firstname'
                            label='First Name'
                            placeholder='First Name'
                          />
                        </div>
                      </div>
                      <div className='col-12 col-sm-6'>
                        <div className='form-group mb-4'>
                          <DefaultTextField className='form-control' type='text' id='lastname' name='lastname' placeholder='Last Name' label='Last Name' />
                        </div>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-12'>
                        <div className='form-group mb-4'>
                          <DefaultTextField className='form-control' type='text' id='telno' name='telno' placeholder='Eg. +66123456789' label='Phone number' />
                        </div>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-12'>
                        <div className='form-group mb-4'>
                          <DefaultTextField className='form-control' type='text' id='email' name='email' placeholder='E-mail' label='E-mail' />
                        </div>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-12 col-sm-6'>
                        <div className='form-group mb-4'>
                          <DefaultTextField className='form-control' type='password' id='password' name='password' placeholder='Password' label='Password' />
                          <small className='form-text' style={{ color: '#B9B9B9', fontSize: '10px' }}>
                            *password must be at least 8 characters*
                          </small>
                        </div>
                      </div>
                      <div className='col-12 col-sm-6'>
                        <div className='form-group mb-4'>
                          <DefaultTextField className='form-control' type='password' id='confirmPassword' name='confirmPassword' placeholder='Confirm Password' label='Confirm Password' />
                        </div>
                      </div>
                    </div>
                    <div className='row mb-3'>
                      <div className='col-12'>
                        <div className='form-check' style={{ color: '#616161' }}>
                          <Checkbox className='form-check-input mr-2' id='acceptTerm' name='acceptTerm' />
                          <label className='form-check-label' htmlFor='acceptTerm' style={{ fontSize: '12px', color: '#616161' }}>
                            By clicking Join now, you agree to OQP User Agreement, Privacy Policy, and Cookie Policy
                          </label>
                        </div>
                        <ErrorMessage name='acceptTerm' />
                      </div>
                    </div>
                    <div>
                      {submitError}
                    </div>
                    <div className='form-group'>
                      <RegisterButton block disabled={submitting} />
                    </div>
                  </form>
                )}
              />
              <LoginFormCardFooter>
                <span>
                  Have an account?
                </span>
                <a className='ml-2' href='/login'>
                  Log In
                </a>
              </LoginFormCardFooter>
            </LoginFormContainer>
          </FormCard>
        </div>
      </LoginPageContainer>
    )
  }
}

export default RegisterPageComponent
