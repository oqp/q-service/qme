import React from 'react'
import propTypes from 'prop-types'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

import Layout from '../layout'
import { EventList } from '../table'
import { CreateEventModal } from '../modal'
import { SecondaryButton } from '../button'

export default class EventListPageComponent extends React.Component {
  static defaultProps = {
    events: null,
  }

  static propTypes = {
    events: propTypes.arrayOf(propTypes.object),
    waitingRoomId: propTypes.string.isRequired,
  }

  constructor(props) {
    super(props)
    const { waitingRoomId } = this.props
    this.createEventModal = withReactContent(Swal).mixin({
      html: (
        <CreateEventModal waitingRoomId={waitingRoomId} />
      ),
      width: '80%',
      padding: '30px 0',
      showCloseButton: false,
      showConfirmButton: false,
      allowEscapeKey: false,
      stopKeydownPropagation: false,
    })
  }

  showModal() {
    this.createEventModal.fire()
  }

  render() {
    const { events } = this.props
    return (
      <Layout
        pageTitle='Events'
        headerNavigation={{ headerTitle: 'Events', previousPageURL: '/panel/waiting-room' }}
      >
        <div className='text-right' style={{ marginBottom: '35px' }}>
          <SecondaryButton type='button' onClick={() => this.showModal()}>+ Create New Event</SecondaryButton>
        </div>
        {!events ? (
          <div className='col text-center'>
            Error
          </div>
        ) : (
          <>
            {events.length === 0 ? (
              <div className='col-12 text-center'>
                There is no event in this collection!
              </div>
            ) : (
              <EventList eventList={events} />
            )}
          </>
        )}
      </Layout>
    )
  }
}
