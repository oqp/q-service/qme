import React from 'react'

import { Form } from 'react-final-form'

import {
  LoginButton, TabButton, FacebookButton, GoogleButton, OutlinedSignUpButton,
} from '../button'
import {
  DefaultTextField, Checkbox,
} from '../input'
import { LoginFormContainer, LoginPageContainer } from '../container'
import { LoginFormTitle } from '../title'
import { Logo } from '../logo'
import { FormCard, LoginFormCardFooter } from '../card'

import logger from '../../config/logger'
import user from '../../services/user'
import formValidation from '../../config/formValidation'

class LoginPageComponent extends React.Component {
  async submitForm(values, form) {
    try {
      const result = await user.loginWithEmail(values)
      if (result && result.password) {
        form.change('password', '')
      }
      return result
    } catch (err) {
      logger.error(err)
      return false
    }
  }

  render() {
    return (
      <LoginPageContainer>
        <div className='d-flex d-md-none'>
          <TabButton className='w-50' href='/signup'>Sign up</TabButton>
          <TabButton className='w-50' active href='/login'>Log in</TabButton>
        </div>
        <div className='pt-3 px-md-5'>
          <h6 className='text-center text-md-left my-3'>
            <a href='/'>
              <Logo src='/static/image/logo.svg' alt='Q-Me logo' />
            </a>
          </h6>
          <h6 className='d-md-none text-center mb-4 text-muted'>
            Integrate, Customize yours.
          </h6>
          <FormCard className='d-md-flex justify-content-center align-items-stretch'>
            <LoginFormContainer className='px-5'>
              <LoginFormTitle className='d-none d-sm-none d-md-block mb-5'>
                Log In
              </LoginFormTitle>
              {/* <div className='row'>
                <div className='col-12 col-lg-6'>
                  <FacebookButton block style={{ marginBottom: '15px' }}>
                    Log In with Facebook
                  </FacebookButton>
                </div>
                <div className='col-12 col-lg-6'>
                  <GoogleButton block style={{ marginBottom: '15px' }}>
                    Log In with Google
                  </GoogleButton>
                </div>
              </div>
              <div className='d-flex justify-content-center align-items-center px-2' style={{ marginBottom: '15px' }}>
                <hr className='w-75' style={{ borderColor: '#D0D0D0' }} />
                <div className='px-2' style={{ color: '#555555' }}>
                  or
                </div>
                <hr className='w-75' style={{ borderColor: '#D0D0D0' }} />
              </div> */}
              <Form
                onSubmit={(values, form) => this.submitForm(values, form)}
                validate={formValidation.loginForm}
                render={({
                  handleSubmit, submitting, submitError,
                }) => (
                  <form onSubmit={handleSubmit}>
                    <div className='row'>
                      <div className='col-12'>
                        <div className='form-group mb-4'>
                          <DefaultTextField type='text' id='email' name='email' placeholder='E-mail' label='E-mail' autoFocus />
                        </div>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-12'>
                        <div className='form-group mb-4'>
                          <DefaultTextField type='password' id='password' name='password' label='Password' placeholder='Password' />
                        </div>
                      </div>
                    </div>
                    <div className='form-group mb-4' style={{ color: '#616161' }}>
                      <Checkbox className='mr-2' id='remember' name='remember' />
                      <label htmlFor='remember' style={{ fontSize: '12px', color: '#616161' }}>Keep me signed in</label>
                    </div>
                    <div>
                      {submitError}
                    </div>
                    <div className='form-group'>
                      <LoginButton disabled={submitting} className='w-100' />
                    </div>
                  </form>
                )}
              />
              <LoginFormCardFooter>
                <span>
                  Don’t have an account?
                </span>
                <a className='ml-2' href='/signup'>
                  Sign Up
                </a>
              </LoginFormCardFooter>
            </LoginFormContainer>
          </FormCard>
        </div>
      </LoginPageContainer>
    )
  }
}

export default LoginPageComponent
