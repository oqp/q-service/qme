import React from 'react'
import styled from 'styled-components'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import PropTypes from 'prop-types'

import Layout from '../layout'
import { WaitingRoomList } from '../table'
import { CreateWaitingRoomModal } from '../modal'
import { SecondaryButton } from '../button'


export default class WaitingRoomListPageComponent extends React.Component {
  static propTypes = {
    waitingRooms: PropTypes.arrayOf(PropTypes.object).isRequired,
  }

  constructor(props) {
    super(props)
    this.createWaitingRoomModal = withReactContent(Swal).mixin({
      html: (
        <CreateWaitingRoomModal />
      ),
      width: '80%',
      padding: '30px 0',
      showCloseButton: false,
      showConfirmButton: false,
      allowEscapeKey: false,
      stopKeydownPropagation: false,
    })
  }

  showModal() {
    this.createWaitingRoomModal.fire()
  }

  render() {
    const { waitingRooms } = this.props
    return (
      <Layout
        pageTitle='Waiting Rooms'
        headerNavigation={{ headerTitle: 'Waiting Rooms', previousPageURL: null }}
      >
        <div className='text-right' style={{ marginBottom: '35px' }}>
          <SecondaryButton type='button' onClick={() => this.showModal()}>+ Create New Waiting Room</SecondaryButton>
        </div>
        {!waitingRooms ? (
          <div className='col text-center'>
            Error
          </div>
        ) : (
          <>
            {waitingRooms.length === 0 ? (
              <div className='col-12 text-center'>
                Create your first waiting room now!
              </div>
            ) : (
              <>
                <WaitingRoomList waitingRoomList={waitingRooms} />
              </>
            )}
          </>
        )}
      </Layout>
    )
  }
}
