import React from 'react'
import propTypes from 'prop-types'

import { DashboardCard } from '../../card'

export default class QueueTimeMetric extends React.Component {
  static defaultProps = {
    eventMetrics: null,
  }

  static propTypes = {
    eventMetrics: propTypes.instanceOf(Object),
    metricKey: propTypes.string.isRequired,
    title: propTypes.string.isRequired,
  }

  render() {
    const { eventMetrics, metricKey, title } = this.props
    let value = '0.0'
    if (eventMetrics && eventMetrics[metricKey].values[0] !== null) {
      const { values } = eventMetrics[metricKey]
      // eslint-disable-next-line prefer-destructuring
      value = `${Math.floor(values[0] / 60)}.${Math.floor(values[0] % 60)}`
    } else {
      value = 'N/A'
    }
    return (
      <div className='d-flex justify-content-center'>
        <DashboardCard title={title}>
          <div style={{ fontSize: '30px' }}>
            {value}
            <span style={{ fontSize: '20px' }}>
              {' (minutes)'}
            </span>
          </div>
        </DashboardCard>
      </div>
    )
  }
}
