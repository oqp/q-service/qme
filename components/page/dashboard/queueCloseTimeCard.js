import React from 'react'
import propTypes from 'prop-types'
import moment from 'moment'

import { DashboardCard } from '../../card'

export default class EventDashboardPageComponent extends React.Component {
  static defaultProps = {
    event: null,
  }

  static propTypes = {
    event: propTypes.instanceOf(Object),
  }

  render() {
    const { event } = this.props
    let cardTitle = ''
    let momentTime
    if (moment().isBefore(event.openTime)) {
      cardTitle = 'Queue will open in'
      momentTime = moment(event.openTime)
    } else if (moment().isBetween(event.openTime, event.closeTime, '[]')) {
      cardTitle = 'Queue will close in'
      momentTime = moment(event.closeTime)
    } else {
      cardTitle = 'Queue has been closed'
      momentTime = moment(event.closeTime)
    }
    return (
      <div className='d-flex justify-content-center h-100'>
        <DashboardCard title={cardTitle}>
          <div style={{ fontSize: '30px' }}>
            {momentTime.fromNow()}
          </div>
          <div style={{ fontSize: '12px' }}>
            {`(${momentTime.format('hh:mma D MMM YYYY')})`}
          </div>
        </DashboardCard>
      </div>
    )
  }
}
