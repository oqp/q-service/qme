/* eslint-disable no-nested-ternary */
import React from 'react'
import propTypes from 'prop-types'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPause, faPlay } from '@fortawesome/free-solid-svg-icons'
import Swal from 'sweetalert2'
import ReactTooltip from 'react-tooltip'
import styled from 'styled-components'
import { DashboardCard } from '../../card'

import eventService from '../../../services/event'

import { queueStatusCalculator } from '../../../util'
import { DefaultButton } from '../../button'

const StatusColorizer = styled.span`
 color: ${props => (props.status === 'Running' ? props.theme.colors.green.default : props.status === 'Paused' ? props.theme.colors.yellow.default : props.theme.colors.darkgrey.default)};
`

export default class EventDashboardPageComponent extends React.Component {
  static defaultProps = {
    event: null,
  }

  static propTypes = {
    event: propTypes.instanceOf(Object),
  }

  constructor(props) {
    super(props)
    this.state = {
      isActive: props.event.isActive,
    }
  }

  async setActive() {
    const { event: { isActive: currentActiveStatus, id, waitingRoomId } } = this.props
    let success = false
    let isPause = false
    if (currentActiveStatus === true) {
      isPause = true
      const result = await eventService.setActiveStatus(id, waitingRoomId, false)
      if (result) {
        this.setState({
          isActive: false,
        })
        success = true
      }
    } else {
      const result = await eventService.setActiveStatus(id, waitingRoomId, true)
      if (result) {
        this.setState({
          isActive: true,
        })
        success = true
      }
    }
    if (success) {
      Swal.fire({
        toast: true,
        type: 'success',
        html: `<div>Successfully ${isPause ? 'Pause' : 'Resume'} the queue.</div>`,
        timer: 5000,
        position: 'top-right',
        showConfirmButton: false,
      })
    } else {
      Swal.fire({
        toast: true,
        type: 'error',
        text: 'Unable to set event state.',
        timer: 5000,
        position: 'top-right',
        showConfirmButton: false,
      })
    }
  }

  render() {
    const { event } = this.props
    const { isActive } = this.state
    const status = queueStatusCalculator(event)
    return (
      <div className='d-flex justify-content-center h-100'>
        <DashboardCard title='Queue Status'>
          <div className='d-flex w-100 justify-content-between'>
            <div style={{ fontSize: '30px' }}>
              <StatusColorizer status={status}>
                {status}
              </StatusColorizer>
            </div>
            {isActive ? (
              <>
                <DefaultButton
                  onClick={() => this.setActive()}
                  data-tip='After you pause the queue,<br/>queue will stop redirecting waiting audiences to the website.'
                >
                  <FontAwesomeIcon icon={faPause} />
                </DefaultButton>
                <ReactTooltip place='top' effect='solid' multiline />
              </>
            ) : (
              <>
                <DefaultButton
                  onClick={() => this.setActive()}
                  data-tip='Press to resume calling waiting audiences to the website'
                >
                  <FontAwesomeIcon icon={faPlay} />
                </DefaultButton>
                <ReactTooltip place='top' effect='solid' multiline />
              </>
            )}
          </div>
        </DashboardCard>
      </div>
    )
  }
}
