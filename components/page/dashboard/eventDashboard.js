import React from 'react'
import propTypes from 'prop-types'
import styled from 'styled-components'

import { Heading3 } from '../../title'

import QueueStatusCard from './queueStatusCard'
import QueueCloseTimeCard from './queueCloseTimeCard'
import QueueAudienceStatusMetricCard from './queueAudienceStatusMetric'

import eventService from '../../../services/event'
import { DashboardCard } from '../../card'
import QueueTimeMetric from './queueTimeMetric'
import QueueNumberMetric from './queueNumberMetric'

import defaultTheme from '../../../theme/default'

const colorSet = {
  backgroundColor: [
    defaultTheme.colors.lightblue.default,
    defaultTheme.colors.green.default,
    defaultTheme.colors.yellow.default,
    defaultTheme.colors.salmon.default,
  ],
}

const CardRow = styled.div`
  padding-left: 20px;

  @media (max-width: 575.98px) {
    padding-left: 0px;
  }
`

export default class EventDashboardPageComponent extends React.Component {
  static defaultProps = {
    event: null,
  }

  static propTypes = {
    event: propTypes.instanceOf(Object),
  }

  async setActive() {
    const { event: { isActive: currentActiveStatus, id, waitingRoomId } } = this.props
    if (currentActiveStatus === true) {
      const result = await eventService.setActiveStatus(id, waitingRoomId, false)
    } else {
      const result = await eventService.setActiveStatus(id, waitingRoomId, true)
    }
  }

  render() {
    const {
      event, eventMetrics, updatedAt, sessionTimedout,
    } = this.props
    if (!event) {
      return (
        <div>
          Getting event data...
        </div>
      )
    }
    return (
      <div>
        <Heading3>
          {'Event: '}
          {event.name}
        </Heading3>
        <CardRow className='row' style={{ marginTop: '48px' }}>
          {sessionTimedout && (
            <div className='alert alert-warning fade show w-100' role='alert'>
              <strong>Your session is timed out.</strong>
              {' '}
              <span>
                Please
                {' '}
                <a href='/login'>log in</a>
                {' '}
                again to continue monitoring
              </span>
            </div>
          )}
          <div className='col-12 text-right text-muted'>
            <span>
              Last update:&nbsp;
            </span>
            <span>
              {updatedAt}
            </span>
          </div>
        </CardRow>
        <CardRow className='row'>
          <div className='col-12 col-md-4 col-sm-6'>
            <QueueStatusCard event={event} />
          </div>
          <div className='col-12 col-md-4 col-sm-6 mt-3 mt-sm-0'>
            <QueueCloseTimeCard event={event} />
          </div>
          <div className='col-12 col-md-4 col-sm-12 mt-3 mt-sm-3 mt-md-0'>
            <div className='d-flex justify-content-center h-100'>
              <DashboardCard title='Current serving queue number'>
                <div style={{ fontSize: '30px' }}>
                  {eventMetrics && eventMetrics.currentQueueNumber.values[0] !== null && eventMetrics.currentQueueNumber.values[0] >= 0 ? eventMetrics.currentQueueNumber.values[0] : 'N/A'}
                </div>
              </DashboardCard>
            </div>
          </div>
        </CardRow>
        <CardRow className='row mt-3'>
          <div className='col-12'>
            <QueueAudienceStatusMetricCard eventMetrics={eventMetrics} colorSet={colorSet} />
          </div>
        </CardRow>
        <CardRow className='row mt-3'>
          <div className='col-6 col-sm-3'>
            <QueueNumberMetric title='Waiting' titleColor={colorSet.backgroundColor[0]} eventMetrics={eventMetrics} metricKey='waitingAudience' />
          </div>
          <div className='col-6 col-sm-3'>
            <QueueNumberMetric title='Completed' titleColor={colorSet.backgroundColor[1]} eventMetrics={eventMetrics} metricKey='completedAudience' />
          </div>
          <div className='col-6 col-sm-3 mt-3 mt-sm-0'>
            <QueueNumberMetric title='Late Arrival' titleColor={colorSet.backgroundColor[2]} eventMetrics={eventMetrics} metricKey='arrivalTimedoutAudience' />
          </div>
          <div className='col-6 col-sm-3 mt-3 mt-sm-0'>
            <QueueNumberMetric title='Session Expired' titleColor={colorSet.backgroundColor[3]} eventMetrics={eventMetrics} metricKey='sessionTimedoutAudience' />
          </div>
        </CardRow>
        <CardRow className='row mt-3'>
          <div className='col-12 col-sm-6'>
            <QueueTimeMetric title='Minimum audience session time' eventMetrics={eventMetrics} metricKey='minSessionTime' />
          </div>
          <div className='col-12 col-sm-6 mt-3 mt-sm-0'>
            <QueueTimeMetric title='Average audience session time' eventMetrics={eventMetrics} metricKey='avgSessionTime' />
          </div>
        </CardRow>
      </div>
    )
  }
}
