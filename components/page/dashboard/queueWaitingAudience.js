import React from 'react'
import propTypes from 'prop-types'

import { DashboardCard } from '../../card'

export default class QueueWaitingAudienceCard extends React.Component {
  static defaultProps = {
    eventMetrics: null,
  }

  static propTypes = {
    eventMetrics: propTypes.instanceOf(Object),
  }

  render() {
    const { eventMetrics } = this.props
    let value = 0
    if (eventMetrics && eventMetrics.waitingAudience.values[0] !== null) {
      const { values } = eventMetrics.waitingAudience
      // eslint-disable-next-line prefer-destructuring
      value = values[0]
    } else {
      value = 'N/A'
    }
    return (
      <div className='d-flex justify-content-center'>
        <DashboardCard title='People waiting in queue'>
          <div style={{ fontSize: '30px' }}>
            {value}
            <span style={{ fontSize: '20px' }}>
              {' (queue)'}
            </span>
          </div>
        </DashboardCard>
      </div>
    )
  }
}
