import React from 'react'
import propTypes from 'prop-types'

import { DashboardCard } from '../../card'

export default class QueueNumberMetric extends React.Component {
  static defaultProps = {
    eventMetrics: null,
    titleColor: '#555555',

  }

  static propTypes = {
    eventMetrics: propTypes.instanceOf(Object),
    metricKey: propTypes.string.isRequired,
    title: propTypes.string.isRequired,
    titleColor: propTypes.string,
  }

  render() {
    const {
      eventMetrics, metricKey, title, titleColor,
    } = this.props
    let value = '0'
    if (eventMetrics && eventMetrics[metricKey].values[0] !== null) {
      const { values } = eventMetrics[metricKey]
      // eslint-disable-next-line prefer-destructuring
      value = values[0]
    } else {
      value = 'N/A'
    }
    return (
      <div className='d-flex justify-content-center h-100'>
        <DashboardCard title={title} titleColor={titleColor}>
          <div style={{ fontSize: '30px' }}>
            <span>{value}</span>
          </div>
        </DashboardCard>
      </div>
    )
  }
}
