import React from 'react'
import propTypes from 'prop-types'

import { Pie } from 'react-chartjs-2'

import { DashboardCardContainer, DashboardCardTitle } from '../../card'

export default class EventDashboardPageComponent extends React.Component {
  static defaultProps = {
    eventMetrics: null,
  }

  static propTypes = {
    eventMetrics: propTypes.instanceOf(Object),
    colorSet: propTypes.arrayOf(propTypes.object).isRequired,
  }

  render() {
    const { eventMetrics, colorSet } = this.props
    return (
      <div className='d-flex justify-content-center'>
        <DashboardCardContainer className='d-none d-sm-flex'>
          <DashboardCardTitle>
            Queue Overview
          </DashboardCardTitle>
          <div className='w-100'>
            {eventMetrics ? (
              <Pie
                height={253}
                options={{
                  maintainAspectRatio: false,
                  legend: {
                    position: 'right',
                    labels: {
                      fontSize: 16,
                      padding: 27,
                      boxWidth: 20,
                    },
                  },
                }}
                data={{
                  labels: [
                    'Waiting',
                    'Completed',
                    'Late Arrival',
                    'Session Expired',
                  ],
                  datasets: [
                    {
                      data: [
                        eventMetrics.waitingAudience.values[0],
                        eventMetrics.completedAudience.values[0],
                        eventMetrics.arrivalTimedoutAudience.values[0],
                        eventMetrics.sessionTimedoutAudience.values[0],
                      ],
                      backgroundColor: colorSet.backgroundColor,
                    },
                  ],
                }}
              />
            ) : (
              'Waiting for data..'
            )}
          </div>
        </DashboardCardContainer>
        <DashboardCardContainer className='d-sm-none'>
          {eventMetrics ? (
            <Pie
              height={253}
              options={{
                maintainAspectRatio: false,
                legend: {
                  position: 'bottom',
                  labels: {
                    fontSize: 16,
                    padding: 27,
                    boxWidth: 20,
                  },
                },
              }}
              data={{
                labels: [
                  'Waiting',
                  'Completed',
                  'Late Arrival',
                  'Session Expired',
                ],
                datasets: [
                  {
                    data: [
                      eventMetrics.waitingAudience.values[0],
                      eventMetrics.completedAudience.values[0],
                      eventMetrics.arrivalTimedoutAudience.values[0],
                      eventMetrics.sessionTimedoutAudience.values[0],
                    ],
                    backgroundColor: colorSet.backgroundColor,
                  },
                ],
              }}
            />
          ) : (
            'Waiting for data..'
          )}
        </DashboardCardContainer>
      </div>
    )
  }
}
