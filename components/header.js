import { useContext } from 'react'
import styled from 'styled-components'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'

import UserContext from './context/userContext'

import userService from '../services/user'

const Container = styled.div`
  position: fixed;
  width: 100%;
  display: flex;
  height: 44px;
`

const Brand = styled.a`
  display: flex;
  max-width: 224px;
  min-width: 224px;
  background-color: #FCAC18;
  font-size: 25px;
  color: #FFF;
  font-weight: bold;
  align-items: center;
  justify-content: center;
  &:link {
    color: #FFF;
    text-decoration: none;
  }
  &:hover {
    color: #FFF;
  }

  @media (max-width: 575.98px) {
    min-width: 75px;
    max-width: 75px;
  }
`

const Navbar = styled.div`
  display: flex;
  width: 100%;
  background-color: #9FD6EE;
  align-items: center;
  justify-content: space-between;
`

const UserInfoContainer = styled.div`
  font-size: 16px;
  margin-right: 16px;
  display: flex;
  align-items: baseline;
`

const ProfileImage = styled.img`
  width: 29px;
  height: 29px;
  border-radius: 50%;
  background-color: #C4C4C4;
  margin-right: 7px;
`

const NavbarTitle = styled.div`
  margin-left: 37px;
  font-size: 20px;
  font-weight: bold;
  color: #000;

  a {
    color: #000;
  }
  a:link {
    color: #000;
  }
  a:hover {
    color: #000;
    text-decoration: none;
  }
`

const Logout = styled.span`
  font-size: 70%;
  margin-left: 7px;
  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }
  color: #555;
`

const Header = ({ headerNavigation }) => {
  const { user } = useContext(UserContext)
  return (
    <Container>
      <Brand href='/panel/waiting-room'>
        OQP
      </Brand>
      <Navbar>
        <NavbarTitle>
          {headerNavigation && (
            <>
              {headerNavigation.previousPageURL ? (
                <a href={headerNavigation.previousPageURL}>
                  <FontAwesomeIcon icon={faChevronLeft} />
                  <span style={{ marginLeft: '18px' }}>
                    {headerNavigation.headerTitle}
                  </span>
                </a>
              ) : (
                <>{headerNavigation.headerTitle}</>
              )}
            </>
          )}
        </NavbarTitle>
        <UserInfoContainer>
          {/* <FontAwesomeIcon icon={faUserCircle} style={{ fontSize: '26px', marginRight: '7px' }} /> */}
          <span className='d-none d-sm-inline'>
            {user.firstname}
          </span>
          <Logout onClick={userService.signout}>
            (Log Out)
          </Logout>
        </UserInfoContainer>
      </Navbar>
    </Container>
  )
}

export default Header
