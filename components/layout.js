import styled from 'styled-components'
import PropTypes from 'prop-types'
import Head from 'next/head'

import React from 'react'
import Header from './header'
import Sidebar from './sidebar'

const Content = styled.div`
  position: relative;
  margin-left: ${props => (props.withSidebar ? '224px' : '0px')};
  flex-grow: 1;
  overflow: auto;
  height: 100%;
  padding: 30px 47px;

  @media (max-width: 575.98px) {
    padding: 30px 13px;
  }
`

const Container = styled.div`
  height: 100vh;
`

const ContentContainer = styled.div`
  padding-top: 44px;
  height: 100vh;
`

export default class Layout extends React.Component {
  render() {
    const {
      children, pageTitle, withSidebar, headerNavigation,
    } = this.props
    return (
      <Container>
        <Head>
          <title>
            {pageTitle}
            {' '}
            | OQP
          </title>
        </Head>
        <Header headerNavigation={headerNavigation} />
        <ContentContainer>
          {withSidebar && <Sidebar />}
          <Content withSidebar={withSidebar}>
            {children}
          </Content>
        </ContentContainer>
      </Container>
    )
  }
}
Layout.defaultProps = {
  withSidebar: false,
}

Layout.propTypes = {
  pageTitle: PropTypes.string.isRequired,
  withSidebar: PropTypes.bool,
}
