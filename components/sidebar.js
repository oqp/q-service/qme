import styled from 'styled-components'
import { SidebarInput } from './input'

const Container = styled.div`
  position: fixed;
  border-right: 1px solid #D0D0D0;
  width: 224px;
  top: 44px;
  bottom: 0;
  height: 100vh;
  overflow-y: visible;
  padding: 18px 17px;
`

const MenuLabel = styled.div`
  font-weight: bold;
`

const SubmenuContainer = styled.div`
  padding-left: 17px;

  & a {
    color: #000;
    text-decoration: none;
  }
`

const menus = [
  {
    label: 'Waiting room',
    submenus: [
      {
        label: 'create',
        icon: 'O',
        href: '/panel/waiting-room/create',
      },
    ],
  },
]

const Sidebar = () => (
  <Container>
    <SidebarInput />
    {menus.map(menu => (
      <div>
        <MenuLabel>
          {menu.label}
        </MenuLabel>
        <SubmenuContainer>
          {menu.submenus.map(submenu => (
            <a href={submenu.href}>
              {submenu.label}
            </a>
          ))}
        </SubmenuContainer>
      </div>
    ))}
  </Container>
)

export default Sidebar
