import moment from 'moment'

export const queueStatusCalculator = (event) => {
  const openTime = moment(event.openTime)
  const closeTime = moment(event.closeTime)
  const now = moment()
  const { isActive } = event
  if (now.isBefore(openTime)) {
    return 'Not Start'
  }
  if (now.isAfter(closeTime)) {
    if (event.isActive === true) {
      return 'Cut off'
    }
    return 'Closed'
  }
  if (now.isBetween(openTime, closeTime, null, '[]')) {
    if (isActive) {
      return 'Running'
    }
    return 'Paused'
  }
  return null
}

export const isEventFieldEditable = (event) => {
  const openTime = moment(event.openTime)
  const now = moment()
  if (now.isBefore(openTime)) {
    return true
  }
  return false
}
