import React from 'react'

import RegisterPageComponent from '../components/page/signup'

class RegisterPage extends React.Component {
  render() {
    return (
      <RegisterPageComponent />
    )
  }
}

export default RegisterPage
