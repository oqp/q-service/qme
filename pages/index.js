import React from 'react'
import styled from 'styled-components'

import { OutlinedButton, LinkButton } from '../components/button'

const Navbar = styled.div`
  position: absolute;
  display: flex;
  width: 100%;
  padding: 43px 43px;

  & .logo {
    width: 88px;
  }
  
  @media (max-width: 575.98px) {
    padding: 20px 20px;

    & .logo {
      width: 48px;
    }
  }

`

const Section1 = styled.div`
  min-height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #555;

  background-image: url('/static/image/wave-bg-animated.svg');
  background-position: right 4rem;
  background-size: contain;
  background-repeat: no-repeat;

  & .trynow {
    width: 50%;
  }

  @media (max-width: 991.98px) {
    background-position-y: 20rem;
    background-size: cover;
  }

  @media (max-width: 767.98px) {
    background-image: url('/static/image/wave-bg.svg');
    padding: 175px 59px 59px;
  }

  @media (max-width: 575.98px) {
    padding: 126px 35px 59px;

    & .trynow {
      width: 100%;
    }
  }
`

class HomePage extends React.Component {
  render() {
    return (
      <div>
        <Navbar>
          <a href='/'>
            <img className='logo' src='/static/image/logo.svg' alt='OQP logo' />
          </a>
          <div className='d-flex justify-content-end w-100'>
            <a href='/signup'>
              <LinkButton>Sign Up</LinkButton>
            </a>
            <a href='/login'>
              <OutlinedButton>Log In</OutlinedButton>
            </a>
          </div>
        </Navbar>
        <Section1>
          <div className='row no-gutters px-md-5'>
            <div className='col-12 col-md-6 text-center'>
              <img src='/static/image/waitingline.svg' alt='waiting line' className='img-fluid animated fadeInUp' />
            </div>
            <div className='col-12 col-md-6 d-flex align-items-center'>
              <div className='animated fadeInRight'>
                <h1 className='text-left' style={{ fontSize: '40px', fontWeight: 'bold' }}>
                  Online Queuing Platform
                </h1>
                <div className='text-left' style={{ fontSize: '25px' }}>
                  A platform that let you gain control over mass incoming user traffics in First-Come-First-Serve queue for your web applications
                </div>
                <div style={{ marginTop: '38px' }}>
                  <a href='/signup'>
                    <OutlinedButton className='trynow'>Try now, it's free</OutlinedButton>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </Section1>
        {/* <div
          className='d-flex align-items-center vh-100'
        >
          <div className='w-100'>
            <div className='text-center' style={{ fontSize: '20px', fontWeight: 'bold' }}>
              Create your own waiting room
            </div>
            <div className='text-center' style={{ fontSize: '17px' }}>
              Change the starting and ending time of queue
            </div>
          </div>
        </div>

        <div
          className='d-flex align-items-center justify-items-center vh-100'
          style={{ backgroundColor: 'rgba(163, 212, 221, 0.64)' }}
        >
          <div className='w-100'>
            <div className='text-center' style={{ fontSize: '20px', fontWeight: 'bold' }}>
              Customize Waiting Room
            </div>
          </div>
        </div> */}
      </div>
    )
  }
}

export default HomePage
