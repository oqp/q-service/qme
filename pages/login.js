import React from 'react'

import LoginPageComponent from '../components/page/login'

class LoginPage extends React.Component {
  render() {
    return <LoginPageComponent />
  }
}

export default LoginPage
