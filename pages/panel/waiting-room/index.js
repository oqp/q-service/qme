import React from 'react'
import { parseCookies } from 'nookies'

import waitingRoomService from '../../../services/waitingRoom'
import WaitingRoomListPageComponent from '../../../components/page/waitingRoomList'

export default class WaitingRoom extends React.Component {
  static async getInitialProps(ctx) {
    const { qme_session: token } = parseCookies(ctx)
    const waitingRooms = await waitingRoomService.getWaitingRoomList(token)
    return { token, waitingRooms }
  }

  render() {
    const { waitingRooms } = this.props
    return (
      <WaitingRoomListPageComponent waitingRooms={waitingRooms} />
    )
  }
}
