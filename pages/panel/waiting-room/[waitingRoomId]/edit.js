import React from 'react'
import EditWaitingRoomForm from '../../../../components/form/waitingRoom/edit'
import Layout from '../../../../components/layout'

import waitingRoomService from '../../../../services/waitingRoom'

export default class EditWaitingRoom extends React.Component {
  static async getInitialProps(ctx) {
    const { waitingRoomId } = ctx.query
    return { waitingRoomId }
  }

  constructor(props) {
    super(props)
    this.state = {
      waitingRoom: null,
    }
  }

  async componentDidMount() {
    const { waitingRoomId } = this.props
    const waitingRoom = await waitingRoomService.getWaitingRoom(waitingRoomId)
    this.setState({ waitingRoom })
  }

  render() {
    const { waitingRoom } = this.state
    return (
      <Layout
        pageTitle='Waiting Rooms'
        headerNavigation={{ headerTitle: 'Waiting Rooms', previousPageURL: '/panel/waiting-room' }}
      >
        <div>
          <ul className='nav nav-tabs'>
            <li className='nav-item'>
              <a className='nav-link active' href='#'>General</a>
            </li>
          </ul>
        </div>
        <div className='pt-4'>
          <EditWaitingRoomForm waitingRoom={waitingRoom} />
        </div>
      </Layout>
    )
  }
}
