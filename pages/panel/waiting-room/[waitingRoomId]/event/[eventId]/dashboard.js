import React from 'react'
import moment from 'moment'

import EventDashboardPageComponent from '../../../../../../components/page/dashboard/eventDashboard'
import Layout from '../../../../../../components/layout'

import eventService from '../../../../../../services/event'
import userService from '../../../../../../services/user'
import logger from '../../../../../../config/logger'

const getEventMetricsMock = async () => {
  const random = Math.random()
  await setTimeout(() => null, 500)
  const eventMetrics = {
    currentQueueNumber: {
      labels: [
        'currentQueueNumber',
      ],
      values: [
        102,
      ],
    },
    maxSessionTime: {
      labels: [
        'maxSessionTime',
      ],
      values: [
        180,
      ],
    },
    minSessionTime: {
      labels: [
        'minSessionTime',
      ],
      values: [
        80,
      ],
    },
    avgSessionTime: {
      labels: [
        'avgSessionTime',
      ],
      values: [
        120,
      ],
    },
    completedAudience: {
      labels: [
        'completedAudience',
      ],
      values: [
        74,
      ],
    },
    sessionTimedoutAudience: {
      labels: [
        'sessionTimedoutAudience',
      ],
      values: [
        4,
      ],
    },
    waitingAudience: {
      labels: [
        'waitingAudience',
      ],
      values: [
        22,
      ],
    },
    arrivalTimedoutAudience: {
      labels: [
        'arrivalTimedoutAudience',
      ],
      values: [
        1,
      ],
    },
    servingAudience: {
      labels: [
        'servingAudience',
      ],
      values: [
        10,
      ],
    },
  }
  if (random < 0.5) {
    return null
  }
  return eventMetrics
}
export default class Dashboard extends React.Component {
  static async getInitialProps(ctx) {
    const { waitingRoomId, eventId } = ctx.query
    return { waitingRoomId, eventId }
  }

  constructor(props) {
    super(props)
    this.state = {
      event: null,
      eventMetrics: null,
      lastEventMetrics: null,
      lastEvent: null,
      updatedAt: null,
      sessionTimedout: false,
    }
  }

  async componentDidMount() {
    this.startPolling()
  }

  async startPolling() {
    const { waitingRoomId, eventId } = this.props
    try {
      const sessionExtended = await userService.extendSession()
      if (sessionExtended === false) {
        this.timer = null
        this.setState({
          sessionTimedout: true,
        })
        return
      }
      const metrics = await eventService.getMetric(eventId, waitingRoomId)
      const event = await eventService.getEvent(eventId, waitingRoomId)
      if (!metrics || !event) {
        const { lastEvent, lastEventMetrics } = this.state
        this.setState({
          event: lastEvent,
          eventMetrics: lastEventMetrics,
        })
      } else {
        this.setState({
          event,
          eventMetrics: metrics,
          lastEventMetrics: metrics,
          lastEvent: event,
          updatedAt: moment().toString(),
        })
      }
    } catch (error) {
      logger.error(error)
    }
    this.timer = setTimeout(async () => this.startPolling(), 10000)
  }

  render() {
    const {
      event, eventMetrics, updatedAt, sessionTimedout,
    } = this.state
    const { waitingRoomId } = this.props
    return (
      <Layout
        pageTitle='Event Dashboard'
        headerNavigation={{
          headerTitle: 'Dashboard',
          previousPageURL: `/panel/waiting-room/${waitingRoomId}`,
        }}
      >
        <EventDashboardPageComponent event={event} eventMetrics={eventMetrics} updatedAt={updatedAt} sessionTimedout={sessionTimedout} />
      </Layout>
    )
  }
}
