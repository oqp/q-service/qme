import React from 'react'
import IntegrationEventForm from '../../../../../../../components/form/event/integration'
import Layout from '../../../../../../../components/layout'

import eventService from '../../../../../../../services/event'
import waitingRoomService from '../../../../../../../services/waitingRoom'

export default class IntegrationEventSetting extends React.Component {
  static async getInitialProps(ctx) {
    const { waitingRoomId, eventId } = ctx.query
    return { waitingRoomId, eventId }
  }

  constructor(props) {
    super(props)
    this.state = {
      event: null,
      waitingRoom: null,
    }
  }

  async componentDidMount() {
    const { waitingRoomId, eventId } = this.props
    const event = eventService.getEvent(eventId, waitingRoomId)
    const waitingRoom = waitingRoomService.getWaitingRoom(waitingRoomId)
    this.setState({ event: await event, waitingRoom: await waitingRoom })
  }

  render() {
    const { event, waitingRoom } = this.state
    const { waitingRoomId, eventId } = this.props
    const webURL = `/panel/waiting-room/${waitingRoomId}/event/${eventId}/setting`
    return (
      <Layout
        pageTitle='Events'
        headerNavigation={{ headerTitle: 'Events', previousPageURL: `/panel/waiting-room/${waitingRoomId}` }}
      >
        <div>
          <ul className='nav nav-tabs'>
            <li className='nav-item'>
              <a className='nav-link' href={`${webURL}/general`}>General</a>
            </li>
            <li className='nav-item'>
              <a className='nav-link active' href={`${webURL}/integration`}>Integration</a>
            </li>
          </ul>
        </div>
        <div className='pt-4'>
          <IntegrationEventForm event={event} waitingRoom={waitingRoom} />
        </div>
      </Layout>
    )
  }
}
