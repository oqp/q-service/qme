import React from 'react'
import EditEventForm from '../../../../../../../components/form/event/edit'
import Layout from '../../../../../../../components/layout'

import eventService from '../../../../../../../services/event'

export default class GeneralEventSetting extends React.Component {
  static async getInitialProps(ctx) {
    const { waitingRoomId, eventId } = ctx.query
    return { waitingRoomId, eventId }
  }

  constructor(props) {
    super(props)
    this.state = {
      event: null,
    }
  }

  async componentDidMount() {
    const { waitingRoomId, eventId } = this.props
    const event = await eventService.getEvent(eventId, waitingRoomId)
    this.setState({ event })
  }

  render() {
    const { event } = this.state
    const { waitingRoomId, eventId } = this.props
    const webURL = `/panel/waiting-room/${waitingRoomId}/event/${eventId}/setting`
    return (
      <Layout
        pageTitle='Events'
        headerNavigation={{ headerTitle: 'Events', previousPageURL: `/panel/waiting-room/${waitingRoomId}` }}
      >
        <div>
          <ul className='nav nav-tabs'>
            <li className='nav-item'>
              <a className='nav-link active' href={`${webURL}/general`}>General</a>
            </li>
            <li className='nav-item'>
              <a className='nav-link' href={`${webURL}/integration`}>Integration</a>
            </li>
          </ul>
        </div>
        <div className='pt-4'>
          <EditEventForm event={event} />
        </div>
      </Layout>
    )
  }
}
