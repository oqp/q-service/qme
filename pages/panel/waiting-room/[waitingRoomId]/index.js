import React from 'react'
import { parseCookies } from 'nookies'
import EventListPageComponent from '../../../../components/page/eventList'

import eventService from '../../../../services/event'

export default class WaitingRoom extends React.Component {
  static async getInitialProps(ctx) {
    const { qme_session: token } = parseCookies(ctx)
    const { waitingRoomId } = ctx.query
    const events = await eventService.getEventList(token, waitingRoomId)
    return { events, waitingRoomId }
  }

  render() {
    const { events, waitingRoomId } = this.props
    return (
      <EventListPageComponent events={events} waitingRoomId={waitingRoomId} />
    )
  }
}
