import NextJsApp, { Container } from 'next/app'
import Head from 'next/head'
import { ThemeProvider, createGlobalStyle } from 'styled-components'
import nookies from 'nookies'
import moment from 'moment'

import UserContext from '../components/context/userContext'

import defaultTheme from '../theme/default'
import '@fortawesome/fontawesome-svg-core/styles.css'
import 'animate.css'

import logger from '../config/logger'
import axios from '../config/axios'

const GlobalStyle = createGlobalStyle`
  body {
    @font-face {
      font-family: ${props => props.theme.primaryFont};
      src: url('${props => props.theme.primaryFontSource}')
    }
    font-family: ${props => props.theme.primaryFont};

    a {
      color: ${props => props.theme.colors.indigo.default};
    }
    a:link {
      color: ${props => props.theme.colors.indigo.default};
    }
    a:hover {
      color: ${props => props.theme.colors.indigo.default};
      text-decoration: underline;
    }
  }
`

const redirectToLogin = (res) => {
  if (res) {
    res.writeHead(302, {
      Location: '/login',
    })
    res.end()
  } else {
    window.location = '/login'
  }
}

const sessionKey = 'qme_session'

class App extends NextJsApp {
  static async getInitialProps(ctx) {
    const { Component, router } = ctx
    let pageProps = {}
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx.ctx)
    }
    if (!['/', '/login', '/signup'].includes(router.asPath)) {
      const { res } = ctx.ctx
      const accessToken = nookies.get(ctx.ctx)[sessionKey]
      if (!accessToken) {
        logger.info('authen failed: no accessToken')
        redirectToLogin(res)
        return { pageProps }
      }
      const response = await axios.get('/user/refreshToken', { Authorization: `Bearer ${accessToken}` })
      if (response.status !== 200) {
        logger.info('authen session expired')
        nookies.destroy(ctx.ctx, sessionKey, { path: '/' })
        redirectToLogin(res)
        return { pageProps }
      }
      const newToken = response.data.data.accessToken
      nookies.destroy(ctx.ctx, sessionKey, { path: '/' })
      const isRemember = nookies.get(ctx.ctx).remember
      let cookieExpireDate
      if (isRemember) {
        cookieExpireDate = moment().add(7, 'd').toDate()
      }
      nookies.set(ctx.ctx, sessionKey, newToken, { path: '/', expires: cookieExpireDate })
      const userResponse = await axios.get('/user/me', { Authorization: `Bearer ${newToken}` })
      if (userResponse.status !== 200) {
        logger.info('cannot get user data')
        redirectToLogin(res)
        return { pageProps }
      }
      const { data } = userResponse.data
      const user = {
        id: data.id,
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
      }
      return { pageProps, user }
    }

    return { pageProps }
  }

  constructor(props) {
    super(props)
    this.state = {
      user: props.user,
    }
  }

  render() {
    const { Component, pageProps } = this.props
    const { user } = this.state
    return (
      <Container>
        <UserContext.Provider value={{ user }}>
          <Head>
            <title>Q-Me</title>
            <link rel='stylesheet' href='/static/css/bootstrap.min.css' />
          </Head>
          <ThemeProvider theme={defaultTheme}>
            <>
              <GlobalStyle />
              <Component {...pageProps} />
            </>
          </ThemeProvider>
        </UserContext.Provider>
      </Container>
    )
  }
}

export default App
