import React from 'react'
import { storiesOf } from '@storybook/react'
import {
  LoginButton, RegisterButton, MainButton, TabButton, FacebookButton, GoogleButton, OutlinedSignUpButton, DefaultButton, DangerButton,
} from '../components/button'

storiesOf('Buttons|Button', module)
  .add('Default Button', () => (
    <DefaultButton>Text</DefaultButton>
  ))
  .add('Main Button', () => (
    <MainButton>Text</MainButton>
  ))
  .add('Danger Button', () => (
    <DangerButton>Text</DangerButton>
  ))
  .add('Login Button', () => (
    <LoginButton />
  ))
  .add('Secondary Login Button', () => (
    <div style={{ backgroundColor: '#9fd6ee', padding: '40px' }}>
      <OutlinedSignUpButton>
        Log in
      </OutlinedSignUpButton>
    </div>
  ))
  .add('Sign up Button', () => (
    <RegisterButton />
  ))
  .add('Secondary Sign up Button', () => (
    <div style={{ backgroundColor: '#9fd6ee', padding: '40px' }}>
      <OutlinedSignUpButton>
        Sign Up
      </OutlinedSignUpButton>
    </div>
  ))
  .add('Facebook Button', () => (
    <FacebookButton>
      Text Goes Here
    </FacebookButton>
  ))
  .add('Google Button', () => (
    <GoogleButton>
      Text Goes Here
    </GoogleButton>
  ))

storiesOf('Buttons|Tab Button', module)
  .add('Active', () => (
    <TabButton href='/login' active>Log in</TabButton>
  ))
  .add('Inactive', () => (
    <TabButton href='/signup'>Sign up</TabButton>
  ))
