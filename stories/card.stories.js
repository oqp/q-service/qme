import React from 'react'
import { storiesOf } from '@storybook/react'
import {
  WaitingRoomCard,
} from '../components/card'

storiesOf('Card|WaitingRoomCard', module)
  .add('WaitingRoomCard', () => (
    <WaitingRoomCard title='ThaiTicketMajor' description='Representative Seller the tickets the 1st in Thailand. Events of special wow one two three.' />
  ))
