import React from 'react'
import { storiesOf } from '@storybook/react'
import {
  PageTitle, LoginFormTitle,
} from '../components/title'

storiesOf('Typography|Title', module)
  .add('Page Title', () => (
    <PageTitle>Text Goes Here</PageTitle>
  ))
  .add('Login or Signup Form Title', () => (
    <LoginFormTitle>Text Goes Here</LoginFormTitle>
  ))
