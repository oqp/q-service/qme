import React from 'react'
import { storiesOf } from '@storybook/react'
import { CreateWaitingRoomModal, CreateEventModal } from '../components/modal'

storiesOf('Modal|Waiting Room', module)
  .add('Create waiting room modal', () => (
    <CreateWaitingRoomModal />
  ))
  .add('Create event modal', () => (
    <CreateEventModal />
  ))
