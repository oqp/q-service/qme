import React from 'react'
import { storiesOf } from '@storybook/react'
import LoginPageComponent from '../components/page/login'
import RegisterPageComponent from '../components/page/signup'

storiesOf('Pages|Login', module)
  .add('Page', () => (
    <LoginPageComponent />
  ))
storiesOf('Pages|Sign Up', module)
  .add('Page', () => (
    <RegisterPageComponent />
  ))
