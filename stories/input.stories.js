import React from 'react'
import { storiesOf } from '@storybook/react'
import { Form, Field } from 'react-final-form'
import {
  DefaultTextField,
  SidebarInput,
  InlineLabelTextField,
  InlineLabelTextArea,
} from '../components/input'

storiesOf('Input|Text Field', module)
  .add('DefaultTextField', () => (
    <Form
      onSubmit
      render={() => (
        <form>
          <DefaultTextField className='form-control' type='text' id='email' name='email' placeholder='E-mail' />
        </form>
      )}
    />
  ))
  .add('SidebarInput', () => (
    <SidebarInput />
  ))
  .add('InlineLabeledTextField', () => (
    <Form
      onSubmit
      render={() => (
        <form>
          <InlineLabelTextField
            label='Label'
            id='field'
            name='field'
            required
            helpText='Helptext goes here.'
          />
        </form>
      )}
    />
  ))
  .add('InlineLabeledTextTextArea', () => (
    <Form
      onSubmit
      render={() => (
        <form>
          <InlineLabelTextArea
            label='Label'
            id='field'
            name='field'
            required
            helpText='Helptext goes here.'
          />
        </form>
      )}
    />
  ))
